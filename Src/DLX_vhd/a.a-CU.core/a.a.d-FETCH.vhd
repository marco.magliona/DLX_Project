library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.myTypes.all;

entity FETCH is
	generic (IR_SIZE: natural;
	REG_ADDR_LEN: natural;
	BIT_NUM : natural;
	OP_CODE_SIZE : natural
);
port ( CLK: in std_logic;
       RST : in std_logic;
       BRANCH_TAKEN : in std_logic;
       MEM_WAIT: out std_logic;
       DRAM_READY: in std_logic;
       INSTR_IN : in std_logic_vector(2*IR_SIZE -1 downto 0);
       INSTR_IN_READY: in std_logic;
       PC_IN : in std_logic_vector(BIT_NUM-1 downto 0);
       NEXT_INSTR : out std_logic;
       NEXT_STAGE_GO : out std_logic;
       INSTR_OUT : out std_logic_vector(IR_SIZE -1 downto 0);
       PC_OUT : out std_logic_vector(BIT_NUM-1 downto 0);
       JUMP_ADDR_IN: in std_logic_vector(2*BIT_NUM+1 downto 0);
       JUMP_ADDR_OUT: out std_logic_vector(BIT_NUM downto 0)
);
end entity FETCH;

architecture arc of FETCH is
	type TYPE_STATE is (
	reset, normal_first_CLK, get_from_queue, give_stored_instr_1, give_stored_instr_2, memory_wait );
	signal CURRENT_STATE, STORED_STATE: TYPE_STATE;
	signal int_NEXT_STAGE_GO: std_logic;
	signal stored_INSTR_OUT_1, stored_INSTR_OUT_2: std_logic_vector(IR_SIZE-1 downto 0);
	signal stored_RF_JAL_1, stored_RF_JAL_2: std_logic_vector(BIT_NUM downto 0);
	signal stored_PC: std_logic_vector(BIT_NUM-1 downto 0);
begin
	NEXT_STAGE_GO <= int_NEXT_STAGE_GO;

	P_FETCH: process(Rst, CLK)
	begin
		if Rst = '0' then
			CURRENT_STATE <= reset;
		else
			if rising_edge(CLK) then
				case CURRENT_STATE is
					when reset =>
						int_NEXT_STAGE_GO <= '0';
						NEXT_INSTR <= '0';
						INSTR_OUT <= (others => '0');
						stored_INSTR_OUT_1 <= (others => '0');
						stored_INSTR_OUT_2 <= (others => '0');
						stored_RF_JAL_1 <= (others => '0');
						stored_RF_JAL_2 <= (others => '0');
						stored_PC <= (others => '0');
					        JUMP_ADDR_OUT <= (others => '0');
						PC_OUT <= (others => '0');
						MEM_WAIT <= '0';
						CURRENT_STATE <= normal_first_CLK;

					when normal_first_CLK =>
						int_NEXT_STAGE_GO <= '0';
						MEM_WAIT <= '0';
						-- This sets the flag in the instr queue so the instruction will be ready as soon as reset is disabled
						NEXT_INSTR <= '1';
						CURRENT_STATE <= get_from_queue;

					when get_from_queue =>
						int_NEXT_STAGE_GO <= '0';
						NEXT_INSTR <= '0';
						CURRENT_STATE <= get_from_queue;
						MEM_WAIT <= '0';
						if(INSTR_IN_READY = '1' and BRANCH_TAKEN = '0') then
							stored_INSTR_OUT_1 <=  INSTR_IN(IR_SIZE-1 downto 0);
							stored_INSTR_OUT_2 <= INSTR_IN(2*IR_SIZE-1 downto IR_SIZE);
							stored_RF_JAL_1 <= JUMP_ADDR_IN(BIT_NUM downto 0);
							stored_RF_JAL_2 <= JUMP_ADDR_IN(2*BIT_NUM+1 downto BIT_NUM+1);
							stored_PC <= PC_IN;
							NEXT_INSTR <= '1';
							INSTR_OUT <= INSTR_IN(IR_SIZE-1 downto 0);
							JUMP_ADDR_OUT <= JUMP_ADDR_IN(BIT_NUM downto 0);
							int_NEXT_STAGE_GO <= '1';
							PC_OUT <= PC_IN;
							if INSTR_IN(BIT_NUM-1 downto BIT_NUM-OP_CODE_SIZE) = OPC_sw or INSTR_IN(BIT_NUM-1 downto BIT_NUM-OP_CODE_SIZE) = OPC_lw  then
								STORED_STATE <= give_stored_instr_2;
								CURRENT_STATE <= memory_wait;
							else
								CURRENT_STATE <= give_stored_instr_2;
							end if;
						end if;

					when give_stored_instr_1 =>
						int_NEXT_STAGE_GO <= '0';
						NEXT_INSTR <= '0';
						if BRANCH_TAKEN = '1' then
							CURRENT_STATE <= get_from_queue;
						else
							INSTR_OUT <= stored_INSTR_OUT_1;
							JUMP_ADDR_OUT <= stored_RF_JAL_1;
							int_NEXT_STAGE_GO <= '1';
							PC_OUT <= stored_PC;
							if stored_INSTR_OUT_1(BIT_NUM-1 downto BIT_NUM-OP_CODE_SIZE) = OPC_sw or stored_INSTR_OUT_1(BIT_NUM-1 downto BIT_NUM-OP_CODE_SIZE) = OPC_lw  then
								STORED_STATE <= give_stored_instr_2;
								CURRENT_STATE <= memory_wait;
							else
								CURRENT_STATE <= give_stored_instr_2;
							end if;
						end if;

					when give_stored_instr_2 =>
						int_NEXT_STAGE_GO <= '0';
						NEXT_INSTR <= '0';
						if BRANCH_TAKEN = '1' then
							CURRENT_STATE <= get_from_queue;
						else
							int_NEXT_STAGE_GO <= '1';
							INSTR_OUT <= stored_INSTR_OUT_2;
							JUMP_ADDR_OUT <= stored_RF_JAL_2;
							PC_OUT <= std_logic_vector(unsigned(stored_PC) + 4);
							if stored_INSTR_OUT_2(BIT_NUM-1 downto BIT_NUM-OP_CODE_SIZE) = OPC_sw or stored_INSTR_OUT_2(BIT_NUM-1 downto BIT_NUM-OP_CODE_SIZE) = OPC_lw  then
								STORED_STATE <= get_from_queue;
								CURRENT_STATE <= memory_wait;
							else
								CURRENT_STATE <= get_from_queue;
							end if;
						end if;

					when memory_wait =>
						MEM_WAIT <= '1';
						int_NEXT_STAGE_GO <= '0';
						NEXT_INSTR <= '0';
						if DRAM_READY = '0' then
							CURRENT_STATE <= memory_wait;
						else
							MEM_WAIT <= '0';
							CURRENT_STATE <= STORED_STATE;
						end if;

					when others => CURRENT_STATE <= reset;
				end case;
			end if;
		end if;
	end process;
end arc;
