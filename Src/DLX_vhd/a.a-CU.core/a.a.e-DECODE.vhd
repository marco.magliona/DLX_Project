library ieee;
use ieee.std_logic_1164.all;
use work.myTypes.all;

entity DECODE is
	generic (IR_SIZE: natural;
	REG_ADDR_LEN: natural;
	OP_CODE_SIZE: natural;
	STAGE_SIZE: natural;
	CW_SIZE : natural;
	BIT_NUM : natural
);
port ( CLK: in std_logic;
       RST : in std_logic;
       BRANCH_TAKEN : in std_logic;
       MEM_WAIT: in std_logic;
       INSTR_IN : in std_logic_vector(IR_SIZE -1 downto 0);
       PREV_STAGE_GO: in std_logic; -- HIGH if previous stage has finished and this needs to start
       NEXT_STAGE_GO : out std_logic;
       INSTR_OUT : out std_logic_vector(IR_SIZE -1 downto 0);
       JUMP_ADDR_IN : in std_logic_vector(BIT_NUM downto 0);
       JUMP_ADDR_OUT : out std_logic_vector(BIT_NUM-1 downto 0);
       STAGE_CW_OUT: out std_logic_vector(STAGE_SIZE-1 downto 0);
       RD_OUT: out std_logic_vector(REG_ADDR_LEN-1 downto 0);
       -- Input register addresses for forwarding unit
       REG_A_ADDR: out std_logic_vector(REG_ADDR_LEN-1 downto 0);
       REG_B_ADDR: out std_logic_vector(REG_ADDR_LEN-1 downto 0)
);
end entity DECODE;

architecture arc of DECODE is

	component CU_MEM_ARRAY is
		generic (OP_CODE_SIZE: natural;
			CW_SIZE: natural);
		port (
		      Rst : in std_logic;
		      OPC: in std_logic_vector(OP_CODE_SIZE-1 downto 0);
		      CW: out std_logic_vector(CW_SIZE-1 downto 0));
	end component CU_MEM_ARRAY;

	signal IR_opcode       : std_logic_vector(OP_CODE_SIZE -1 downto 0);  -- OpCode part of IR
	signal IR_func         : std_logic_vector(FUNC_SIZE-1 downto 0);   -- Func part of IR when Rtype
	signal int_ALU_OP      : std_logic_vector(ALU_OPC_SIZE-1 downto 0);
	signal IR_RD_ADDR      : std_logic_vector(REG_ADDR_LEN-1 downto 0);
	signal int_cw          : std_logic_vector(CW_SIZE - 1 downto 0); -- full control word read from cw_mem
	signal int_INSTR_OUT: std_logic_vector(IR_SIZE-1 downto 0);
begin

	IR_opcode(5 downto 0) <= INSTR_IN(31 downto 26);
	IR_func(10 downto 0)  <= INSTR_IN(FUNC_SIZE - 1 downto 0);
	IR_RD_ADDR <= INSTR_IN(15 downto 11) when IR_opcode = OPC_R_TYPE else
		      INSTR_IN(20 downto 16); -- TODO CHECKME

	CU_MEM_ARRAY_0 : CU_MEM_ARRAY
	generic map (
			    OP_CODE_SIZE => OP_CODE_SIZE,
		    	    CW_SIZE => CW_SIZE)
	port map (
			 Rst => Rst,
			 OPC => IR_opcode,
			 CW => int_cw );

	-- ALU_OP_CODE generation
	ALU_OP_GEN: process (Rst, IR_opcode, IR_func)
	begin
		if Rst = '0' then
			int_ALU_OP <= (others => '0');
		else
			if(IR_opcode = OPC_R_TYPE) then
			--RTYPE functions
				if(IR_func(OP_CODE_SIZE-1 downto 0) = OPC_add)  then  int_ALU_OP <= ALU_ADD ;
				elsif(IR_func(OP_CODE_SIZE-1 downto 0) = OPC_addu) then  int_ALU_OP <= ALU_ADD ;
				elsif(IR_func(OP_CODE_SIZE-1 downto 0) = OPC_and)  then  int_ALU_OP <= ALU_AND ;
				elsif(IR_func(OP_CODE_SIZE-1 downto 0) = OPC_or )  then  int_ALU_OP <= ALU_OR ;
				elsif(IR_func(OP_CODE_SIZE-1 downto 0) = OPC_seq)  then  int_ALU_OP <= ALU_E ;
				elsif(IR_func(OP_CODE_SIZE-1 downto 0) = OPC_sge)  then  int_ALU_OP <= ALU_GE ;
				elsif(IR_func(OP_CODE_SIZE-1 downto 0) = OPC_sgt)  then  int_ALU_OP <= ALU_G  ;
				elsif(IR_func(OP_CODE_SIZE-1 downto 0) = OPC_sle)  then  int_ALU_OP <= ALU_LE ;
				elsif(IR_func(OP_CODE_SIZE-1 downto 0) = OPC_sll)  then  int_ALU_OP <= ALU_SLL ;
				elsif(IR_func(OP_CODE_SIZE-1 downto 0) = OPC_slt)  then  int_ALU_OP <= ALU_L ;
				elsif(IR_func(OP_CODE_SIZE-1 downto 0) = OPC_sne)  then  int_ALU_OP <= ALU_NE ;
				elsif(IR_func(OP_CODE_SIZE-1 downto 0) = OPC_sra)  then  int_ALU_OP <= ALU_SRA ;
				elsif(IR_func(OP_CODE_SIZE-1 downto 0) = OPC_srl)  then  int_ALU_OP <= ALU_SRL ;
				elsif(IR_func(OP_CODE_SIZE-1 downto 0) = OPC_sub)  then  int_ALU_OP <= ALU_SUB ;
				elsif(IR_func(OP_CODE_SIZE-1 downto 0) = OPC_xor)  then  int_ALU_OP <= ALU_XOR ;
				else
					int_ALU_OP <= (others => '0');
				end if;
		-- Immediate Artih/logic
			elsif(IR_opcode = OPC_addi)        then  int_ALU_OP <= ALU_ADD ;
			elsif(IR_opcode = OPC_addui)       then  int_ALU_OP <= ALU_ADD ;
			elsif(IR_opcode = OPC_andi)        then  int_ALU_OP <= ALU_AND ;
			elsif(IR_opcode = OPC_ori )        then  int_ALU_OP <= ALU_OR ;
			elsif(IR_opcode = OPC_seqi)        then  int_ALU_OP <= ALU_E ;
			elsif(IR_opcode = OPC_sgei)        then  int_ALU_OP <= ALU_GE ;
			elsif(IR_opcode = OPC_sgti)        then  int_ALU_OP <= ALU_G  ;
			elsif(IR_opcode = OPC_slei)        then  int_ALU_OP <= ALU_LE ;
			elsif(IR_opcode = OPC_slli)        then  int_ALU_OP <= ALU_SLL ;
			elsif(IR_opcode = OPC_slti)        then  int_ALU_OP <= ALU_L ;
			elsif(IR_opcode = OPC_snei)        then  int_ALU_OP <= ALU_NE ;
			elsif(IR_opcode = OPC_srli)        then  int_ALU_OP <= ALU_SRL ;
			elsif(IR_opcode = OPC_subi)        then  int_ALU_OP <= ALU_SUB ;
			elsif(IR_opcode = OPC_subui)       then  int_ALU_OP <= ALU_SUB ;
			elsif(IR_opcode = OPC_xori)        then  int_ALU_OP <= ALU_XOR ;
		-- Jump (ALU does  an add)
			elsif(IR_opcode = OPC_beqz)        then  int_ALU_OP <= ALU_ADD ;
			elsif(IR_opcode = OPC_bnez)        then  int_ALU_OP <= ALU_ADD ;
			elsif(IR_opcode = OPC_jr)          then  int_ALU_OP <= ALU_ADD ; -- Here it does nothing
											 -- LOAD/Store
			elsif(IR_opcode = OPC_lw)          then  int_ALU_OP <= ALU_ADD ;
			elsif(IR_opcode = OPC_sw)          then  int_ALU_OP <= ALU_ADD ;
			else
				int_ALU_OP <= ALU_ADD;
			end if;
		end if;
	end process;


	REG_A_ADDR <= int_INSTR_OUT(25 downto 21);
	REG_B_ADDR <= int_INSTR_OUT(20 downto 16);
	INSTR_OUT <= int_INSTR_OUT;

	P_DECODE: process(Rst, CLK)
	begin
		if Rst = '0' then
			NEXT_STAGE_GO <= '0';
			STAGE_CW_OUT <= (others => '0');
			int_INSTR_OUT <= (others => '0');
			RD_OUT <= (others => '0');
			JUMP_ADDR_OUT <= (others => '0');
		else
			if rising_edge(CLK) then
				NEXT_STAGE_GO <= '0';
				STAGE_CW_OUT <= (others => '0');
				int_INSTR_OUT <= (others => '0');
				RD_OUT <= (others => '0');
				JUMP_ADDR_OUT <= (others => '0');
				if(PREV_STAGE_GO = '1' and BRANCH_TAKEN = '0') then
					STAGE_CW_OUT <= int_cw;
					STAGE_CW_OUT(POS_CW_ALU_OP+ALU_OPC_SIZE-1 downto POS_CW_ALU_OP) <= int_ALU_OP;
							-- Set the correct sign bit here if needed (R type instructions have the same entry in the memory)
					if(IR_opcode = OPC_R_TYPE) then
						if((IR_func(OP_CODE_SIZE-1 downto 0) = OPC_addu) or (IR_func(OP_CODE_SIZE-1 downto 0) = OPC_subu)) then
							STAGE_CW_OUT(POS_CW_IMM_SIGN) <= '1';
						end if;
					end if;
					STAGE_CW_OUT(POS_CW_WR_RET_ADDR) <= JUMP_ADDR_IN(BIT_NUM);
					JUMP_ADDR_OUT <= JUMP_ADDR_IN(BIT_NUM-1 downto 0);
					int_INSTR_OUT <= INSTR_IN;
					RD_OUT <= IR_RD_ADDR;
					NEXT_STAGE_GO <= '1';
				end if;

			end if;
		end if;
	end process;
end arc;

