library ieee;
use ieee.std_logic_1164.all;
use work.myTypes.all;
use ieee.numeric_std.all;

entity WRITE_BACK is
	generic (STAGE_SIZE: natural;
		 REG_ADDR_LEN : natural
		);
	port ( CLK: in std_logic;
	       RST : in std_logic;
	       BRANCH_TAKEN : in std_logic;
	       MEM_WAIT: in std_logic;
	       STAGE_CW_IN: in std_logic_vector(STAGE_SIZE-1 downto 0);
               RD_IN : in std_logic_vector(REG_ADDR_LEN-1 downto 0);
	       PREV_STAGE_GO: in std_logic; -- HIGH if previous stage has finished and this needs to start
	       STAGE_CW_OUT: out std_logic_vector(STAGE_SIZE-1 downto 0);
       	       RD_OUT : out std_logic_vector(REG_ADDR_LEN-1 downto 0)
       );
end entity WRITE_BACK;

architecture arc of WRITE_BACK is
begin

	P_WRITE_BACK: process(Rst, CLK)
	begin
		if Rst = '0' then
			STAGE_CW_OUT <= (others => '0');
			RD_OUT <= (others => '0');
		end if;
		if rising_edge(CLK) then
			RD_OUT <= (others => '0');
			STAGE_CW_OUT <= (others => '0');
			if PREV_STAGE_GO = '1' then
				STAGE_CW_OUT <= STAGE_CW_IN;
				RD_OUT <= RD_IN;
			end if;
		end if;
	end process;
end arc;
