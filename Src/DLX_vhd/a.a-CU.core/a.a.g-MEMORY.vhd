library ieee;
use ieee.std_logic_1164.all;
use work.myTypes.all;

entity MEMORY is
	generic (STAGE_SIZE: natural;
	 	 REG_ADDR_LEN : natural
		);
	port ( CLK: in std_logic;
	       RST : in std_logic;
	       BRANCH_TAKEN : in std_logic;
	       MEM_WAIT: in std_logic;
	       STAGE_CW_IN: in std_logic_vector(STAGE_SIZE-1 downto 0);
       	       RD_IN : in std_logic_vector(REG_ADDR_LEN-1 downto 0);
	       PREV_STAGE_GO: in std_logic; -- HIGH if previous stage has finished and this needs to start
	       NEXT_STAGE_GO : out std_logic;
	       STAGE_CW_OUT: out std_logic_vector(STAGE_SIZE-1 downto 0);
       	       RD_OUT : out std_logic_vector(REG_ADDR_LEN-1 downto 0);
	--Memory interface
	       DRAM_ENABLE: out std_logic;
	       DRAM_READY: in std_logic
       );
end entity MEMORY;

architecture arc of MEMORY is
	type TYPE_STATE is (
	reset, normal, memory_wait_ready
);
signal CURRENT_STATE: TYPE_STATE;
signal int_DRAM_ENABLE: std_logic;
begin

DRAM_ENABLE <= int_DRAM_ENABLE;

	P_MEMORY: process(Rst, CLK)
	begin
		if Rst = '0' then
			CURRENT_STATE <= reset;
		else
			if rising_edge(CLK) then
				case CURRENT_STATE is
					when reset =>
						int_DRAM_ENABLE <= '0';
						NEXT_STAGE_GO <= '0';
						STAGE_CW_OUT <= (others => '0');
						RD_OUT <= (others => '0');
						CURRENT_STATE <= normal;

					when normal =>
						int_DRAM_ENABLE <= '0';
						NEXT_STAGE_GO <= '0';
						STAGE_CW_OUT <= (others => '0');
						RD_OUT <= (others => '0');
						CURRENT_STATE <= normal;
						if(PREV_STAGE_GO = '1') then
							STAGE_CW_OUT <= STAGE_CW_IN;
							RD_OUT <= RD_IN;
							if(STAGE_CW_IN(CW_IND_STAGE3_SZ+POS_FF_DRAM_WE) = '1' or STAGE_CW_IN(CW_IND_STAGE3_SZ+POS_FF_REG_LMD_LATCH_EN) = '1') then
								int_DRAM_ENABLE <= '1';
								CURRENT_STATE <= memory_wait_ready;
							else
								NEXT_STAGE_GO <= '1';
							end if;
						end if;

					when memory_wait_ready  =>
						int_DRAM_ENABLE <= '0';
						NEXT_STAGE_GO <= '0';
						if(DRAM_READY = '0') then
							int_DRAM_ENABLE <= '1';
							CURRENT_STATE <= memory_wait_ready;
						else
							NEXT_STAGE_GO <= '1';
							CURRENT_STATE <= normal;
						end if;

					when others => CURRENT_STATE <= reset;
				end case;
			end if;
		end if;
	end process;

end arc;
