library ieee;
use ieee.std_logic_1164.all;
use work.myTypes.all;
use work.utils.all;
use ieee.numeric_std.all;

-- Here are stored the control words for the various operations
entity CU_MEM_ARRAY is
	generic (OP_CODE_SIZE: natural;
		 CW_SIZE: natural);
	port (
	      Rst: in std_logic;
	      OPC: in std_logic_vector(OP_CODE_SIZE-1 downto 0);
	      CW: out std_logic_vector(CW_SIZE-1 downto 0));
end CU_MEM_ARRAY;

architecture struct of CU_MEM_ARRAY is

	subtype cw_type is std_logic_vector(CW_SIZE -1 downto 0);
	type mem_array is array (integer range <>) of cw_type;

	signal cw_mem : mem_array(0 to 2**OP_CODE_SIZE-1) := (others => (others => '0'));
	signal R_TYPE : cw_type;
	signal signed_I_TYPE : cw_type;
	signal unsigned_I_TYPE : cw_type;

begin

	R_TYPE <= generate_cw_no_alu_op(
	REG_A => '1',REG_B => '1',IMM_SIGN => '0',REG_IMM => '0',MUXB => '1',
	COND_JUMP => '0',EQ_COND => '0',ALU_OUT => '1',JUMP_EN => '0',JUMP_REG => '0',DRAM_WE => '0',LMD => '0',WB_MUX => '0',RF_WE => '1', WR_RET_ADDR => '0');

	signed_I_TYPE <= generate_cw_no_alu_op(
	REG_A => '1',REG_B => '0',IMM_SIGN => '0',REG_IMM => '1',MUXB => '0',
	COND_JUMP => '0',EQ_COND => '0',ALU_OUT => '1',JUMP_EN => '0',JUMP_REG => '0',DRAM_WE => '0',LMD => '0',WB_MUX => '0',RF_WE => '1', WR_RET_ADDR => '0');

	unsigned_I_TYPE <= generate_cw_no_alu_op(
	REG_A => '1',REG_B => '0',IMM_SIGN => '1',REG_IMM => '1',MUXB => '0',
	COND_JUMP => '0',EQ_COND => '0',ALU_OUT => '1',JUMP_EN => '0',JUMP_REG => '0',DRAM_WE => '0',LMD => '0',WB_MUX => '0',RF_WE => '1', WR_RET_ADDR => '0');

	cw_mem(OPC_NAT_R_TYPE) <= R_TYPE;

	cw_mem(OPC_NAT_addi) <= signed_I_TYPE;
	cw_mem(OPC_NAT_andi) <= signed_I_TYPE;
	cw_mem(OPC_NAT_ori ) <= signed_I_TYPE;
	cw_mem(OPC_NAT_seqi) <= signed_I_TYPE;
	cw_mem(OPC_NAT_sgei) <= signed_I_TYPE;
	cw_mem(OPC_NAT_sgti) <= signed_I_TYPE;
	cw_mem(OPC_NAT_slei) <= signed_I_TYPE;
	cw_mem(OPC_NAT_slli) <= signed_I_TYPE;
	cw_mem(OPC_NAT_slti) <= signed_I_TYPE;
	cw_mem(OPC_NAT_snei) <= signed_I_TYPE;
	cw_mem(OPC_NAT_srli) <= signed_I_TYPE;
	cw_mem(OPC_NAT_subi) <= signed_I_TYPE;
	cw_mem(OPC_NAT_xori) <= signed_I_TYPE;

	cw_mem(OPC_NAT_addui) <= unsigned_I_TYPE;
	cw_mem(OPC_NAT_subui) <= unsigned_I_TYPE;

	cw_mem(OPC_NAT_beqz) <= generate_cw_no_alu_op(
				REG_A => '1',REG_B => '0',IMM_SIGN => '0',REG_IMM => '1',MUXB => '0',
				COND_JUMP => '1',EQ_COND => '1',ALU_OUT => '1',JUMP_EN => '1',JUMP_REG => '0',DRAM_WE => '0',LMD => '0',WB_MUX => '0',RF_WE => '0', WR_RET_ADDR => '0');

	cw_mem(OPC_NAT_bnez) <= generate_cw_no_alu_op(
				REG_A => '1',REG_B => '0',IMM_SIGN => '0',REG_IMM => '1',MUXB => '0',
				COND_JUMP => '1',EQ_COND => '0',ALU_OUT => '1',JUMP_EN => '1',JUMP_REG => '0',DRAM_WE => '0',LMD => '0',WB_MUX => '0',RF_WE => '0', WR_RET_ADDR => '0');

	cw_mem(OPC_NAT_jr) <= generate_cw_no_alu_op(
			      REG_A => '1',REG_B => '0',IMM_SIGN => '0',REG_IMM => '0',MUXB => '0',
			      COND_JUMP => '0',EQ_COND => '0',ALU_OUT => '0',JUMP_EN => '1',JUMP_REG => '1',DRAM_WE => '0',LMD => '0',WB_MUX => '0',RF_WE => '0', WR_RET_ADDR => '0');

	cw_mem(OPC_NAT_lw) <= generate_cw_no_alu_op(
			      REG_A => '1',REG_B => '0',IMM_SIGN => '0',REG_IMM => '1',MUXB => '0',
			      COND_JUMP => '0',EQ_COND => '0',ALU_OUT => '1',JUMP_EN => '0',JUMP_REG => '0',DRAM_WE => '0',LMD => '1',WB_MUX => '1',RF_WE => '1', WR_RET_ADDR => '0');

	cw_mem(OPC_NAT_nop) <= generate_cw_no_alu_op(
			       REG_A => '0',REG_B => '0',IMM_SIGN => '0',REG_IMM => '0',MUXB => '0',
			       COND_JUMP => '0',EQ_COND => '0',ALU_OUT => '0',JUMP_EN => '0',JUMP_REG => '0',DRAM_WE => '0',LMD => '0',WB_MUX => '0',RF_WE => '0', WR_RET_ADDR => '0');

	cw_mem(OPC_NAT_sw) <= generate_cw_no_alu_op(
			      REG_A => '1',REG_B => '1',IMM_SIGN => '0',REG_IMM => '1',MUXB => '0',
			      COND_JUMP => '0',EQ_COND => '0',ALU_OUT => '1',JUMP_EN => '0',JUMP_REG => '0',DRAM_WE => '1',LMD => '0',WB_MUX => '0',RF_WE => '0', WR_RET_ADDR => '0');

	CW <= cw_mem(to_integer(unsigned(OPC))) when Rst ='1' else (others => '0');
end architecture struct;

