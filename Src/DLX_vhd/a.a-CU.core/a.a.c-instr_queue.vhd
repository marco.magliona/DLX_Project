library ieee;
use ieee.std_logic_1164.all;
use work.myTypes.all;
use work.utils.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

entity INSTR_QUEUE is
	generic (IR_SIZE : natural := 32;
		 QUEUE_SIZE: natural := 8;
		 BIT_NUM : natural := 32);
	port (
		     CLK: in std_logic;
		     RST: in std_logic;
		     BRANCH_TAKEN: in std_logic;
		     IRAM_ENABLE : out std_logic;
		     IRAM_READY : in std_logic;
		     IRAM_ADDRESS : out std_logic_vector(BIT_NUM-1 downto 0);
		     -- When RST is HIGH, get the PC value and start reading data from IRAM at address PC
		     PC_IN : in std_logic_vector(BIT_NUM-1 downto 0);
		     IR_IN : in std_logic_vector(2*IR_SIZE - 1 downto 0);
		     IR_OUT : out std_logic_vector(2*IR_SIZE -1 downto 0);
		     PC_OUT : out std_logic_vector(BIT_NUM-1 downto 0);
		     JUMP_ADDR : out std_logic_vector(2*BIT_NUM+1 downto 0);
		     NEXT_INSTR: in std_logic;
		     IR_OUT_READY : out std_logic
	     );
end INSTR_QUEUE;

architecture arc of INSTR_QUEUE is

	type FIFO_INSTR is array(0 to QUEUE_SIZE-1) of std_logic_vector(2*IR_SIZE-1 downto 0);
	type STORED_PC is array(0 to QUEUE_SIZE-1) of std_logic_vector(2*BIT_NUM-1 downto 0);
	type STORED_PC_jal is array(0 to QUEUE_SIZE-1) of std_logic_vector(2*BIT_NUM+1 downto 0); -- 1 bit more to control the write enable of the register
	signal instr_queue: FIFO_INSTR;
	-- Store the program counter associated with that instruction with the instruction
	signal pc_queue: STORED_PC;
	signal jump_queue: STORED_PC_jal;
	signal backward_cond_jump_in_instr : std_logic_vector(QUEUE_SIZE-1 downto 0);

	signal int_IRAM_ENABLE : std_logic;
	signal int_PC : std_logic_vector(BIT_NUM-1 downto 0);
	signal int_PC_jump : std_logic_vector(2*BIT_NUM+1 downto 0);

	signal int_IR_IN: std_logic_vector(2*IR_SIZE-1 downto 0);
	signal get_from_IRAM_ready : std_logic;
	signal backward_cond_jump_issued: std_logic;

	-- 1 bit more (MSB) to differentiate empty and full statuses
	signal head, tail, loop_head, loop_jump_back_pos: std_logic_vector(integer(ceil(log2(real(QUEUE_SIZE)))) downto 0);
	-- loop_jump_back_pos contains the position in the queue of the conditional backward jump instruction
	-- loop_head contains the position of the instruction pointed by the backward jump
begin

	IRAM_ENABLE <= int_IRAM_ENABLE;
	IRAM_ADDRESS <= "00"&int_PC(BIT_NUM-1 downto 2) ;

	FIFO_PROCESS_GET_FROM_IRAM: process(Rst, BRANCH_TAKEN, CLK)
	begin
		if rising_edge(CLK) then
			if RST = '0' then
				int_IRAM_ENABLE <= '0';
				get_from_IRAM_ready <= '0';
			else
				int_IRAM_ENABLE <= '1';
				get_from_IRAM_ready <= '0';
				-- FILL the queue, read from IRAM until full
				if((head(head'left-1 downto 0) = tail(tail'left-1 downto 0) and head(head'left) = tail(tail'left)) or --Check if full
				head(head'left-1 downto 0) /= tail(tail'left-1 downto 0)) then -- Check not full and not empty
					-- If a possible loop is detected, do not overwrite the instructions in the loop
					if not(loop_head(loop_head'left) = '1' and loop_head(loop_head'left-1 downto 0) = head(head'left-1 downto 0)) then
						if(IRAM_READY = '1' and int_IRAM_ENABLE = '1') then
							int_IR_IN <= IR_IN;
							get_from_IRAM_ready <= '1';
						end if;
					end if;
				end if;
			end if;
		end if;
	end process;

	FIFO_PROCESS_CHECK_JUMP: process(Rst, BRANCH_TAKEN, CLK)
		variable int_OP_CODE_1, int_OP_CODE_2: std_logic_vector(OP_CODE_SIZE-1 downto 0);
		variable found : std_logic;
		variable cond_jump_address: std_logic_vector(BIT_NUM-1 downto 0);
	begin
		if RST = '0' then
			for i in 0 to QUEUE_SIZE-1 loop
				instr_queue(i) <= OPC_nop&(IR_SIZE-OPC_nop'length-1 downto 0 => '0')&OPC_nop&(IR_SIZE-OPC_nop'length-1 downto 0 => '0');
				pc_queue(i) <= (others  => '0');
				jump_queue(i) <= (others => '0');
				backward_cond_jump_in_instr(i) <= '0';
			end loop;
			head <= (others => '0');
			int_PC <= PC_IN;
			int_PC_jump <= (others => '0');
		elsif BRANCH_TAKEN = '1' then
			found := '0';
			CHECK_IF_PRESENT: for i in QUEUE_SIZE-1 downto 0 loop
				if pc_queue(i)(BIT_NUM-1 downto 0) = PC_IN then
					found := '1';
				elsif pc_queue(i)(2*BIT_NUM-1 downto BIT_NUM)  = PC_IN then
					found := '1';
					instr_queue(i)(IR_SIZE-1 downto 0) <= OPC_nop&(IR_SIZE-OPC_nop'length-1 downto 0 => '0');
				end if;
			end loop;
			if found = '0' then
				for i in 0 to QUEUE_SIZE-1 loop
					instr_queue(i) <= OPC_nop&(IR_SIZE-OPC_nop'length-1 downto 0 => '0')&OPC_nop&(IR_SIZE-OPC_nop'length-1 downto 0 => '0');
					pc_queue(i) <= (others  => '0');
					jump_queue(i) <= (others => '0');
				end loop;
				head <= (others => '0');
				int_PC <= PC_IN;
				int_PC_jump <= (others => '0');
			end if;
		elsif rising_edge(CLK) then
			if get_from_IRAM_ready = '1' then
				int_OP_CODE_1 := int_IR_IN(IR_SIZE-1 downto IR_SIZE-OP_CODE_SIZE);
				int_OP_CODE_2 := int_IR_IN(2*IR_SIZE-1 downto 2*IR_SIZE-OP_CODE_SIZE);
			-- Check if unconditional JUMP (j) in the first instruction
				if int_OP_CODE_1 = OPC_j or int_OP_CODE_1 = OPC_jal then
				-- It's a jump in the first instruction, remove both, update the int_PC and fetch the instruction
				-- at the address specified by the jump (At the next CLK cycle)
					int_PC <= std_logic_vector(signed(int_PC) + signed(int_IR_IN(IR_SIZE-OP_CODE_SIZE-1 downto 0)) + 4);
					if int_OP_CODE_1 = OPC_jal then
						int_PC_jump(BIT_NUM downto 0) <= '1'&std_logic_vector(unsigned(int_PC) + 8);
					end if;
			-- Check if unconditional JUMP (j) in the second instruction
				elsif int_OP_CODE_2 = OPC_j or int_OP_CODE_2 = OPC_jal then
				-- It's a jump in the second instruction
				-- The first instruction should be executed anyway so overwrite the second
				-- instruction with a nop and update the int_PC to the one specified in the
				-- jump. You've been unlucky here.
					int_PC <= std_logic_vector(signed(int_PC) + signed(int_IR_IN(2*IR_SIZE-OP_CODE_SIZE-1 downto IR_SIZE)) + 8);
					instr_queue(to_integer(unsigned(head(head'left-1 downto 0)))) <= int_IR_IN;
					instr_queue(to_integer(unsigned(head(head'left-1 downto 0))))(2*IR_SIZE-1 downto 2*IR_SIZE-OP_CODE_SIZE) <= OPC_nop;
					pc_queue(to_integer(unsigned(head(head'left-1 downto 0)))) <= std_logic_vector(unsigned(int_PC) + 4)&int_PC;
					if int_OP_CODE_2 = OPC_jal then
						int_PC_jump(2*BIT_NUM+1 downto BIT_NUM+1) <= '1'&std_logic_vector(unsigned(int_PC) + 12);
					end if;
					head <= std_logic_vector(unsigned(head) + 1);
				else
					jump_queue(to_integer(unsigned(head(head'left-1 downto 0)))) <= int_PC_jump;
				-- Any other instruction is queued
					instr_queue(to_integer(unsigned(head(head'left-1 downto 0)))) <= int_IR_IN;
					pc_queue(to_integer(unsigned(head(head'left-1 downto 0)))) <= std_logic_vector(unsigned(int_PC) + 4)&int_PC;
					int_PC <= std_logic_vector(unsigned(int_PC) + 8);
					int_PC_jump <= (others => '0');
					head <= std_logic_vector(unsigned(head) + 1);
					backward_cond_jump_in_instr(to_integer(unsigned(head(head'left-1 downto 0)))) <= '0';
				-- Check if conditional jump in the first instruction, compute and store the jump destination address
					if int_OP_CODE_1 = OPC_beqz or int_OP_CODE_1 = OPC_bnez then
						cond_jump_address := std_logic_vector(signed(int_PC) + signed(int_IR_IN(15 downto 0)) + 4);
						jump_queue(to_integer(unsigned(head(head'left-1 downto 0))))(BIT_NUM downto 0) <= '0'&cond_jump_address;
						-- Decide if this is a backward jump
						backward_cond_jump_in_instr(to_integer(unsigned(head(head'left-1 downto 0)))) <= int_IR_IN(15);
				-- Check if conditional jump in the second instruction, compute and store the jump destination address
					elsif int_OP_CODE_2 = OPC_beqz or int_OP_CODE_2 = OPC_bnez then
						cond_jump_address := std_logic_vector(signed(int_PC) + signed(int_IR_IN(15+IR_SIZE downto IR_SIZE)) + 8);
						jump_queue(to_integer(unsigned(head(head'left-1 downto 0))))(2*BIT_NUM+1 downto BIT_NUM+1) <= '0'&cond_jump_address;
						-- Decide if this is a backward jump
						backward_cond_jump_in_instr(to_integer(unsigned(head(head'left-1 downto 0)))) <= int_IR_IN(15+IR_SIZE);
					end if;
				end if;
			end if;
		end if;
	end process;

	FIFO_PROCESS_SPILL: process(Rst, BRANCH_TAKEN, CLK)
		variable int_NEXT_INSTR : std_logic;
		variable int_IR_OUT: std_logic_vector(IR_SIZE-1 downto 0);
	begin
		if rising_edge(CLK) then
			if RST = '0' or BRANCH_TAKEN = '1' then
				tail <= (others => '0');
				IR_OUT <= OPC_nop&(IR_SIZE-OPC_nop'length-1 downto 0 => '0')&OPC_nop&(IR_SIZE-OPC_nop'length-1 downto 0 => '0');
				IR_OUT_READY <= '0';
				PC_OUT <= (others => '0');
				JUMP_ADDR <= (others => '0');
				backward_cond_jump_issued <= '0';
				loop_jump_back_pos <= (others => '0');
				-- In case of a branch load directly the next instruction so it will be already available when requested
				if BRANCH_TAKEN = '1' then
					int_NEXT_INSTR := '1';
					-- Look for the destination address of the jump, if it's in the queue give it directly otherwise flush the queue and fetch from IRAM
					CHECK_IF_PRESENT: for i in QUEUE_SIZE-1 downto 0 loop
						if pc_queue(i)(BIT_NUM-1 downto 0) = PC_IN then
							tail(tail'left-1 downto 0) <= std_logic_vector(to_unsigned(i,tail'left));
							-- Force full flag if head and tail have the same value so to not overwrite the cell that needs to be given
							tail(tail'left) <= not(head(head'left));
							if backward_cond_jump_issued = '1' then
								loop_head <= '1'&std_logic_vector(to_unsigned(i,tail'left));
							end if;
						elsif pc_queue(i)(2*BIT_NUM-1 downto BIT_NUM)  = PC_IN then
							tail(tail'left-1 downto 0) <= std_logic_vector(to_unsigned(i,tail'left));
							-- Force full flag if head and tail have the same value so to not overwrite the cell that needs to be given
							tail(tail'left) <= not(head(head'left));
							if backward_cond_jump_issued = '1' then
								loop_head <= '1'&std_logic_vector(to_unsigned(i,tail'left));
							end if;
						end if;
					end loop;
				else
					int_NEXT_INSTR := '0';
				end if;
			else
				-- If the CU asks for a new instruction, the one on the output has already been read so a new one is loading
				if NEXT_INSTR = '1' then
					int_NEXT_INSTR := '1';
					IR_OUT_READY <= '0';
				end if;

				-- SPILL the queue when requested until empty
				if((head(head'left-1 downto 0) = tail(tail'left-1 downto 0) and head(head'left) /= tail(tail'left)) or -- Check if empty
				head(head'left-1 downto 0) /= tail(tail'left-1 downto 0)) then --Check not full and not empty
					if(int_NEXT_INSTR = '1') then
						IR_OUT <= instr_queue(to_integer(unsigned(tail(tail'left-1 downto 0))));
						PC_OUT <= pc_queue(to_integer(unsigned(tail(tail'left-1 downto 0))))(BIT_NUM-1 downto 0);
						JUMP_ADDR <= jump_queue(to_integer(unsigned(tail(tail'left-1 downto 0))));
						tail <= std_logic_vector(unsigned(tail) + 1);
						-- Check if the jump has not been taken, out of the loop
						if std_logic_vector(unsigned(loop_jump_back_pos(loop_jump_back_pos'left -1 downto 0)) + 3) = tail(tail'left-1 downto 0) then
							loop_head(loop_head'left) <= '0';
							backward_cond_jump_issued <= '0';
						end if;
						if backward_cond_jump_in_instr(to_integer(unsigned(tail(tail'left-1 downto 0)))) = '1' then
							loop_jump_back_pos <= '0'&tail(tail'left-1 downto 0);
							backward_cond_jump_issued <= '1';
						end if;
					end if;
					IR_OUT_READY <= '1';
					int_NEXT_INSTR := '0';
				end if;
			end if;
		end if;
	end process;
end arc;

