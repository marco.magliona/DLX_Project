library ieee;
use ieee.std_logic_1164.all;
use work.myTypes.all;
use work.utils.all;
use ieee.numeric_std.all;

-- This expects to be used at decode phase

entity FOR_UNIT is
	port (
		     CLK: in std_logic;
		     RST: in std_logic;
		     RD_IN: in RF_ADDR_TYPE(3 downto 0); -- FROM DECODE (position 0), EXECUTE, MEMORY, WRITE BACK stages
		     RD_REQUIRED: in RF_ADDR_TYPE(1 downto 0);
		     MUX_SEL: out FOR_MUX_SEL_TYPE_ARRAY(1 downto 0)
	     );
end FOR_UNIT;

architecture arc of FOR_UNIT is
signal int_MUX_SEL: FOR_MUX_SEL_TYPE_ARRAY(1 downto 0);
begin

	MUX_SEL <= int_MUX_SEL;
	process(RST, CLK)
	begin
		if(RST = '0') then
			int_MUX_SEL(0) <= (others => '0');
			int_MUX_SEL(1) <= (others => '0');
		else
			if(rising_edge(CLK)) then
				int_MUX_SEL(0) <= FOR_NONE;
				int_MUX_SEL(1) <= FOR_NONE;
				compare_rd_req_with_each_stage_rd: for j in 0 to 3 loop
					compare_reg_a_and_b: for i in 1 downto 0 loop
						if(unsigned(RD_REQUIRED(i)) /= 0) then
							if(RD_IN(j) = RD_REQUIRED(i)) then
								int_MUX_SEL(i) <= std_logic_vector(to_unsigned(j,FOR_MUX_SEL'length));
							end if;
						end if;
					end loop;
				end loop;
			end if;
		end if;
	end process;
end arc;

