library ieee;
use ieee.std_logic_1164.all;
use work.myTypes.all;

package utils is
	-- Generate CW for the data path starting from the single control signals (useful for testbench)
	function generate_cw
	(constant REG_A, REG_B, IMM_SIGN, REG_IMM,  WR_RET_ADDR, MUXB, COND_JUMP, EQ_COND, ALU_OUT, JUMP_EN, JUMP_REG, DRAM_WE, LMD,WB_MUX,RF_WE : in std_logic ;
	ALU_OP : in std_logic_vector(ALU_OPC_SIZE-1 downto 0))
	return std_logic_vector;
	-- Generate CW for the data path starting from the single control signals without alu opcode (useful for CU memory)
	function generate_cw_no_alu_op (constant REG_A, REG_B, IMM_SIGN, REG_IMM, WR_RET_ADDR, MUXB, COND_JUMP, EQ_COND, ALU_OUT, JUMP_EN, JUMP_REG, DRAM_WE, LMD,WB_MUX,RF_WE : in std_logic)
	return std_logic_vector;
end package utils;

package body utils is
	function generate_cw (constant REG_A, REG_B, IMM_SIGN, REG_IMM, WR_RET_ADDR, MUXB, COND_JUMP, EQ_COND, ALU_OUT, JUMP_EN, JUMP_REG, DRAM_WE, LMD,WB_MUX,RF_WE : in std_logic ;
	ALU_OP:  in std_logic_vector(ALU_OPC_SIZE-1 downto 0))
	return std_logic_vector is
		variable CW: std_logic_vector(CW_SIZE-1 downto 0);
	begin

		CW(POS_CW_REG_A_LATCH_EN      ) := REG_A ;
		CW(POS_CW_REG_B_LATCH_EN      ) := REG_B ;
		CW(POS_CW_REG_B_DELAYED_LATCH_EN) := REG_B ; -- This should always folloe REG_B
		CW(POS_CW_IMM_SIGN            ) := IMM_SIGN ;
		CW(POS_CW_WR_RET_ADDR         ) := WR_RET_ADDR;
		CW(POS_CW_REG_IMM_LATCH_EN    ) := REG_IMM ;
		CW(POS_CW_MUXB_SEL            ) := MUXB ;
		CW(POS_CW_COND_JUMP           ) := COND_JUMP ;
		CW(POS_CW_EQ_COND             ) := EQ_COND ;
		CW(POS_CW_JUMP_REG            ) := JUMP_REG;
		CW(POS_CW_REG_ALU_OUT_LATCH_EN) := ALU_OUT ;
		CW(POS_CW_ALU_OP+ALU_OPC_SIZE-1 downto POS_CW_ALU_OP) := ALU_OP ;
		CW(POS_CW_JUMP_EN             ) := JUMP_EN ;
		CW(POS_CW_DRAM_WE             ) := DRAM_WE ;
		CW(POS_CW_REG_LMD_LATCH_EN    ) := LMD ;
		CW(POS_CW_REG_ALU_DEL_LATCH_EN) := ALU_OUT; -- This should always follow ALU_OUT
		CW(POS_CW_FOR_MUX_MEM         ) := WB_MUX ;
		CW(POS_CW_WB_MUX_SEL          ) := WB_MUX ;
		CW(POS_CW_RF_WE               ) := RF_WE ;

		return CW;
	end function generate_cw;

	function generate_cw_no_alu_op (constant REG_A, REG_B, IMM_SIGN, REG_IMM, WR_RET_ADDR, MUXB, COND_JUMP, EQ_COND, ALU_OUT, JUMP_EN, JUMP_REG, DRAM_WE, LMD,WB_MUX,RF_WE : in std_logic)
	return std_logic_vector is
	begin
	return generate_cw (REG_A, REG_B, IMM_SIGN, REG_IMM, WR_RET_ADDR, MUXB, COND_JUMP, EQ_COND, ALU_OUT, JUMP_EN, JUMP_REG, DRAM_WE, LMD,WB_MUX,RF_WE, (others => '0'));
	end function generate_cw_no_alu_op;
end utils;
