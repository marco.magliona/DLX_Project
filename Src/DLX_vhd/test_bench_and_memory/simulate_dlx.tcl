# Run this script from "test_bench" folder using relative paths
vlib work

source ./compile_procedures.tcl
compile_global_files
compile_datapath
compile_dlx_cu

#Compile memory files
foreach file [glob ./TB_packages/*.vhd] {
	vcom -reportprogress 300 -work work $file
}

foreach file [glob ./TB_romem/*.vhd] {
	vcom -reportprogress 300 -work work $file
}

foreach file [glob ./TB_rwmem/*.vhd] {
	vcom -reportprogress 300 -work work $file
}

#Compile TOP level
vcom -reportprogress 300 -work work ../a-DLX.vhd
#Compile Testbench
vcom -reportprogress 300 -work work TB_TOP_DLX.vhd

vsim -t 10ps -novopt work.dlx_testbench

#add wave *
do wave_dlx.do

run 500 ns
