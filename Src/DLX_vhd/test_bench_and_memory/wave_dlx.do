onerror {resume}
quietly virtual signal -install /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0 { /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/tail(2 downto 0)} tail_pos
quietly virtual signal -install /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0 { /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/head(2 downto 0)} head_pos
quietly WaveActivateNextPane {} 0
add wave -noupdate /dlx_testbench/CLK
add wave -noupdate /dlx_testbench/RST
add wave -noupdate /dlx_testbench/IRAM_ADDRESS
add wave -noupdate /dlx_testbench/IRAM_ENABLE
add wave -noupdate /dlx_testbench/IRAM_READY
add wave -noupdate /dlx_testbench/IRAM_DATA
add wave -noupdate /dlx_testbench/DRAM_ADDRESS
add wave -noupdate /dlx_testbench/DRAM_ENABLE
add wave -noupdate /dlx_testbench/DRAM_READNOTWRITE
add wave -noupdate /dlx_testbench/DRAM_READY
add wave -noupdate /dlx_testbench/DRAM_DATA
add wave -noupdate /dlx_testbench/DLX_0/BRANCH_TAKEN
add wave -noupdate -group IRAM /dlx_testbench/IRAM/CLK
add wave -noupdate -group IRAM /dlx_testbench/IRAM/RST
add wave -noupdate -group IRAM /dlx_testbench/IRAM/ADDRESS
add wave -noupdate -group IRAM /dlx_testbench/IRAM/ENABLE
add wave -noupdate -group IRAM /dlx_testbench/IRAM/DATA_READY
add wave -noupdate -group IRAM /dlx_testbench/IRAM/DATA
add wave -noupdate -group IRAM /dlx_testbench/IRAM/Memory
add wave -noupdate -group IRAM /dlx_testbench/IRAM/valid
add wave -noupdate -group IRAM /dlx_testbench/IRAM/idout
add wave -noupdate -group IRAM /dlx_testbench/IRAM/count
add wave -noupdate -group DRAM /dlx_testbench/DRAM/CLK
add wave -noupdate -group DRAM /dlx_testbench/DRAM/RST
add wave -noupdate -group DRAM /dlx_testbench/DRAM/ADDR
add wave -noupdate -group DRAM /dlx_testbench/DRAM/ENABLE
add wave -noupdate -group DRAM /dlx_testbench/DRAM/READNOTWRITE
add wave -noupdate -group DRAM /dlx_testbench/DRAM/DATA_READY
add wave -noupdate -group DRAM /dlx_testbench/DRAM/INOUT_DATA
add wave -noupdate -group DRAM /dlx_testbench/DRAM/DRAM_mem
add wave -noupdate -group DRAM /dlx_testbench/DRAM/tmp_data
add wave -noupdate -group DRAM /dlx_testbench/DRAM/int_data_ready
add wave -noupdate -group DRAM /dlx_testbench/DRAM/mem_ready
add wave -noupdate -group DRAM /dlx_testbench/DRAM/counter
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/CLK
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/RST
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/IRAM_ENABLE
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/IRAM_READY
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/IRAM_ADDRESS
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/PC_IN
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/IR_IN
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/IR_OUT
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/PC_OUT
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/JUMP_ADDR
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/NEXT_INSTR
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/IR_OUT_READY
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/instr_queue
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/pc_queue
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/jump_queue
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/int_PC
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/head
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/tail
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/head_pos
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/tail_pos
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/backward_cond_jump_issued
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/loop_head
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/loop_jump_back_pos
add wave -noupdate -group dlx_cu_0 -group INSTR_QUEUE /dlx_testbench/DLX_0/dlx_cu_0/INSTR_QUEUE_0/backward_cond_jump_in_instr
add wave -noupdate -group dlx_cu_0 -group FOR_UNIT /dlx_testbench/DLX_0/dlx_cu_0/FOR_UNIT_0/CLK
add wave -noupdate -group dlx_cu_0 -group FOR_UNIT /dlx_testbench/DLX_0/dlx_cu_0/FOR_UNIT_0/RST
add wave -noupdate -group dlx_cu_0 -group FOR_UNIT /dlx_testbench/DLX_0/dlx_cu_0/FOR_UNIT_0/ENABLE
add wave -noupdate -group dlx_cu_0 -group FOR_UNIT /dlx_testbench/DLX_0/dlx_cu_0/FOR_UNIT_0/RD_IN
add wave -noupdate -group dlx_cu_0 -group FOR_UNIT /dlx_testbench/DLX_0/dlx_cu_0/FOR_UNIT_0/RD_REQUIRED
add wave -noupdate -group dlx_cu_0 -group FOR_UNIT -expand /dlx_testbench/DLX_0/dlx_cu_0/FOR_UNIT_0/int_MUX_SEL
add wave -noupdate -group dlx_cu_0 -group FOR_UNIT -expand /dlx_testbench/DLX_0/dlx_cu_0/FOR_UNIT_0/MUX_SEL
add wave -noupdate -group dlx_cu_0 -group FETCH /dlx_testbench/DLX_0/dlx_cu_0/FETCH_0/CLK
add wave -noupdate -group dlx_cu_0 -group FETCH /dlx_testbench/DLX_0/dlx_cu_0/FETCH_0/RST
add wave -noupdate -group dlx_cu_0 -group FETCH /dlx_testbench/DLX_0/dlx_cu_0/FETCH_0/BRANCH_TAKEN
add wave -noupdate -group dlx_cu_0 -group FETCH /dlx_testbench/DLX_0/dlx_cu_0/FETCH_0/MEM_WAIT
add wave -noupdate -group dlx_cu_0 -group FETCH /dlx_testbench/DLX_0/dlx_cu_0/FETCH_0/INSTR_IN
add wave -noupdate -group dlx_cu_0 -group FETCH /dlx_testbench/DLX_0/dlx_cu_0/FETCH_0/INSTR_IN_READY
add wave -noupdate -group dlx_cu_0 -group FETCH /dlx_testbench/DLX_0/dlx_cu_0/FETCH_0/NEXT_INSTR
add wave -noupdate -group dlx_cu_0 -group FETCH /dlx_testbench/DLX_0/dlx_cu_0/FETCH_0/NEXT_STAGE_GO
add wave -noupdate -group dlx_cu_0 -group FETCH /dlx_testbench/DLX_0/dlx_cu_0/FETCH_0/INSTR_OUT
add wave -noupdate -group dlx_cu_0 -group FETCH /dlx_testbench/DLX_0/dlx_cu_0/FETCH_0/CURRENT_STATE
add wave -noupdate -group dlx_cu_0 -group FETCH /dlx_testbench/DLX_0/dlx_cu_0/FETCH_0/JUMP_ADDR_IN
add wave -noupdate -group dlx_cu_0 -group FETCH /dlx_testbench/DLX_0/dlx_cu_0/FETCH_0/JUMP_ADDR_OUT
add wave -noupdate -group dlx_cu_0 -group DECODE -group CU_MEM_ARRAY /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/CU_MEM_ARRAY_0/Rst
add wave -noupdate -group dlx_cu_0 -group DECODE -group CU_MEM_ARRAY /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/CU_MEM_ARRAY_0/OPC
add wave -noupdate -group dlx_cu_0 -group DECODE -group CU_MEM_ARRAY /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/CU_MEM_ARRAY_0/CW
add wave -noupdate -group dlx_cu_0 -group DECODE -group CU_MEM_ARRAY /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/CU_MEM_ARRAY_0/cw_mem
add wave -noupdate -group dlx_cu_0 -group DECODE -group CU_MEM_ARRAY /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/CU_MEM_ARRAY_0/R_TYPE
add wave -noupdate -group dlx_cu_0 -group DECODE -group CU_MEM_ARRAY /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/CU_MEM_ARRAY_0/signed_I_TYPE
add wave -noupdate -group dlx_cu_0 -group DECODE -group CU_MEM_ARRAY /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/CU_MEM_ARRAY_0/unsigned_I_TYPE
add wave -noupdate -group dlx_cu_0 -group DECODE /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/CLK
add wave -noupdate -group dlx_cu_0 -group DECODE /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/RST
add wave -noupdate -group dlx_cu_0 -group DECODE /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/BRANCH_TAKEN
add wave -noupdate -group dlx_cu_0 -group DECODE /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/MEM_WAIT
add wave -noupdate -group dlx_cu_0 -group DECODE /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/INSTR_IN
add wave -noupdate -group dlx_cu_0 -group DECODE /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/PREV_STAGE_GO
add wave -noupdate -group dlx_cu_0 -group DECODE /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/NEXT_STAGE_GO
add wave -noupdate -group dlx_cu_0 -group DECODE /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/INSTR_OUT
add wave -noupdate -group dlx_cu_0 -group DECODE /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/STAGE_CW_OUT
add wave -noupdate -group dlx_cu_0 -group DECODE /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/IR_opcode
add wave -noupdate -group dlx_cu_0 -group DECODE /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/IR_func
add wave -noupdate -group dlx_cu_0 -group DECODE /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/int_ALU_OP
add wave -noupdate -group dlx_cu_0 -group DECODE /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/IR_RD_ADDR
add wave -noupdate -group dlx_cu_0 -group DECODE /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/int_cw
add wave -noupdate -group dlx_cu_0 -group DECODE /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/JUMP_ADDR_IN
add wave -noupdate -group dlx_cu_0 -group DECODE /dlx_testbench/DLX_0/dlx_cu_0/DECODE_0/JUMP_ADDR_OUT
add wave -noupdate -group dlx_cu_0 -group EXECUTE /dlx_testbench/DLX_0/dlx_cu_0/EXECUTE_0/CLK
add wave -noupdate -group dlx_cu_0 -group EXECUTE /dlx_testbench/DLX_0/dlx_cu_0/EXECUTE_0/RST
add wave -noupdate -group dlx_cu_0 -group EXECUTE /dlx_testbench/DLX_0/dlx_cu_0/EXECUTE_0/BRANCH_TAKEN
add wave -noupdate -group dlx_cu_0 -group EXECUTE /dlx_testbench/DLX_0/dlx_cu_0/EXECUTE_0/MEM_WAIT
add wave -noupdate -group dlx_cu_0 -group EXECUTE /dlx_testbench/DLX_0/dlx_cu_0/EXECUTE_0/STAGE_CW_IN
add wave -noupdate -group dlx_cu_0 -group EXECUTE /dlx_testbench/DLX_0/dlx_cu_0/EXECUTE_0/PREV_STAGE_GO
add wave -noupdate -group dlx_cu_0 -group EXECUTE /dlx_testbench/DLX_0/dlx_cu_0/EXECUTE_0/NEXT_STAGE_GO
add wave -noupdate -group dlx_cu_0 -group EXECUTE /dlx_testbench/DLX_0/dlx_cu_0/EXECUTE_0/STAGE_CW_OUT
add wave -noupdate -group dlx_cu_0 -group MEMORY /dlx_testbench/DLX_0/dlx_cu_0/MEMORY_0/CLK
add wave -noupdate -group dlx_cu_0 -group MEMORY /dlx_testbench/DLX_0/dlx_cu_0/MEMORY_0/RST
add wave -noupdate -group dlx_cu_0 -group MEMORY /dlx_testbench/DLX_0/dlx_cu_0/MEMORY_0/BRANCH_TAKEN
add wave -noupdate -group dlx_cu_0 -group MEMORY /dlx_testbench/DLX_0/dlx_cu_0/MEMORY_0/MEM_WAIT
add wave -noupdate -group dlx_cu_0 -group MEMORY /dlx_testbench/DLX_0/dlx_cu_0/MEMORY_0/STAGE_CW_IN
add wave -noupdate -group dlx_cu_0 -group MEMORY /dlx_testbench/DLX_0/dlx_cu_0/MEMORY_0/PREV_STAGE_GO
add wave -noupdate -group dlx_cu_0 -group MEMORY /dlx_testbench/DLX_0/dlx_cu_0/MEMORY_0/NEXT_STAGE_GO
add wave -noupdate -group dlx_cu_0 -group MEMORY /dlx_testbench/DLX_0/dlx_cu_0/MEMORY_0/STAGE_CW_OUT
add wave -noupdate -group dlx_cu_0 -group MEMORY /dlx_testbench/DLX_0/dlx_cu_0/MEMORY_0/DRAM_ENABLE
add wave -noupdate -group dlx_cu_0 -group MEMORY /dlx_testbench/DLX_0/dlx_cu_0/MEMORY_0/DRAM_READY
add wave -noupdate -group dlx_cu_0 -group MEMORY /dlx_testbench/DLX_0/dlx_cu_0/MEMORY_0/CURRENT_STATE
add wave -noupdate -group dlx_cu_0 -group WRITE_BACK /dlx_testbench/DLX_0/dlx_cu_0/WRITE_BACK_0/CLK
add wave -noupdate -group dlx_cu_0 -group WRITE_BACK /dlx_testbench/DLX_0/dlx_cu_0/WRITE_BACK_0/RST
add wave -noupdate -group dlx_cu_0 -group WRITE_BACK /dlx_testbench/DLX_0/dlx_cu_0/WRITE_BACK_0/BRANCH_TAKEN
add wave -noupdate -group dlx_cu_0 -group WRITE_BACK /dlx_testbench/DLX_0/dlx_cu_0/WRITE_BACK_0/MEM_WAIT
add wave -noupdate -group dlx_cu_0 -group WRITE_BACK /dlx_testbench/DLX_0/dlx_cu_0/WRITE_BACK_0/STAGE_CW_IN
add wave -noupdate -group dlx_cu_0 -group WRITE_BACK /dlx_testbench/DLX_0/dlx_cu_0/WRITE_BACK_0/PREV_STAGE_GO
add wave -noupdate -group dlx_cu_0 -group WRITE_BACK /dlx_testbench/DLX_0/dlx_cu_0/WRITE_BACK_0/STAGE_CW_OUT
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/CLK
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/Rst
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/IR_IN
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/IR_OUT
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/int_IR_OUT
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/RF_WR_ADDR
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/OVERFLOW
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/CW
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/IRAM_ENABLE
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/IRAM_READY
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/stage2_in_s
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/stage3_in_s
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/stage4_in_s
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/stage5_in_s
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/stage1_out_s
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/stage2_out_s
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/stage3_out_s
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/stage4_out_s
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/int_MEM_WAIT
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/FOR_MUX_SEL_SIGNALS
add wave -noupdate -group dlx_cu_0 /dlx_testbench/DLX_0/dlx_cu_0/JUMP_ADDR_OUT
add wave -noupdate -group data_path -group RF_B_DELAYED /dlx_testbench/DLX_0/data_path_core_0/RF_B_DELAYED/D
add wave -noupdate -group data_path -group RF_B_DELAYED /dlx_testbench/DLX_0/data_path_core_0/RF_B_DELAYED/CLK
add wave -noupdate -group data_path -group RF_B_DELAYED /dlx_testbench/DLX_0/data_path_core_0/RF_B_DELAYED/LATCH_EN
add wave -noupdate -group data_path -group RF_B_DELAYED /dlx_testbench/DLX_0/data_path_core_0/RF_B_DELAYED/RESET
add wave -noupdate -group data_path -group RF_B_DELAYED /dlx_testbench/DLX_0/data_path_core_0/RF_B_DELAYED/Q
add wave -noupdate -group data_path -group RF_ALU_OUT /dlx_testbench/DLX_0/data_path_core_0/RF_ALU_OUT/D
add wave -noupdate -group data_path -group RF_ALU_OUT /dlx_testbench/DLX_0/data_path_core_0/RF_ALU_OUT/CLK
add wave -noupdate -group data_path -group RF_ALU_OUT /dlx_testbench/DLX_0/data_path_core_0/RF_ALU_OUT/LATCH_EN
add wave -noupdate -group data_path -group RF_ALU_OUT /dlx_testbench/DLX_0/data_path_core_0/RF_ALU_OUT/RESET
add wave -noupdate -group data_path -group RF_ALU_OUT /dlx_testbench/DLX_0/data_path_core_0/RF_ALU_OUT/Q
add wave -noupdate -group data_path -group RF_ALU_OUT_DELAYED /dlx_testbench/DLX_0/data_path_core_0/RF_ALU_OUT_DELAYED/D
add wave -noupdate -group data_path -group RF_ALU_OUT_DELAYED /dlx_testbench/DLX_0/data_path_core_0/RF_ALU_OUT_DELAYED/CLK
add wave -noupdate -group data_path -group RF_ALU_OUT_DELAYED /dlx_testbench/DLX_0/data_path_core_0/RF_ALU_OUT_DELAYED/LATCH_EN
add wave -noupdate -group data_path -group RF_ALU_OUT_DELAYED /dlx_testbench/DLX_0/data_path_core_0/RF_ALU_OUT_DELAYED/RESET
add wave -noupdate -group data_path -group RF_ALU_OUT_DELAYED /dlx_testbench/DLX_0/data_path_core_0/RF_ALU_OUT_DELAYED/Q
add wave -noupdate -group data_path -group RF_IN_DELAYED /dlx_testbench/DLX_0/data_path_core_0/RF_RF_IN_DELAYED/D
add wave -noupdate -group data_path -group RF_IN_DELAYED /dlx_testbench/DLX_0/data_path_core_0/RF_RF_IN_DELAYED/CLK
add wave -noupdate -group data_path -group RF_IN_DELAYED /dlx_testbench/DLX_0/data_path_core_0/RF_RF_IN_DELAYED/LATCH_EN
add wave -noupdate -group data_path -group RF_IN_DELAYED /dlx_testbench/DLX_0/data_path_core_0/RF_RF_IN_DELAYED/RESET
add wave -noupdate -group data_path -group RF_IN_DELAYED /dlx_testbench/DLX_0/data_path_core_0/RF_RF_IN_DELAYED/Q
add wave -noupdate -group data_path -group RF_LMD /dlx_testbench/DLX_0/data_path_core_0/RF_LMD/D
add wave -noupdate -group data_path -group RF_LMD /dlx_testbench/DLX_0/data_path_core_0/RF_LMD/CLK
add wave -noupdate -group data_path -group RF_LMD /dlx_testbench/DLX_0/data_path_core_0/RF_LMD/LATCH_EN
add wave -noupdate -group data_path -group RF_LMD /dlx_testbench/DLX_0/data_path_core_0/RF_LMD/RESET
add wave -noupdate -group data_path -group RF_LMD /dlx_testbench/DLX_0/data_path_core_0/RF_LMD/Q
add wave -noupdate -group data_path -group dlx_rf -group RF /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/register_file/CLK
add wave -noupdate -group data_path -group dlx_rf -group RF /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/register_file/RESET
add wave -noupdate -group data_path -group dlx_rf -group RF /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/register_file/ENABLE
add wave -noupdate -group data_path -group dlx_rf -group RF /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/register_file/RD1
add wave -noupdate -group data_path -group dlx_rf -group RF /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/register_file/RD2
add wave -noupdate -group data_path -group dlx_rf -group RF /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/register_file/WR
add wave -noupdate -group data_path -group dlx_rf -group RF /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/register_file/WR_RET_ADDR
add wave -noupdate -group data_path -group dlx_rf -group RF /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/register_file/RET_ADDR_IN
add wave -noupdate -group data_path -group dlx_rf -group RF /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/register_file/ADD_WR
add wave -noupdate -group data_path -group dlx_rf -group RF /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/register_file/ADD_RD1
add wave -noupdate -group data_path -group dlx_rf -group RF /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/register_file/ADD_RD2
add wave -noupdate -group data_path -group dlx_rf -group RF /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/register_file/DATAIN
add wave -noupdate -group data_path -group dlx_rf -group RF /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/register_file/OUT1
add wave -noupdate -group data_path -group dlx_rf -group RF /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/register_file/OUT2
add wave -noupdate -group data_path -group dlx_rf -group RF /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/register_file/REGISTERS
add wave -noupdate -group data_path -group dlx_rf /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/CLK
add wave -noupdate -group data_path -group dlx_rf /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/RESET
add wave -noupdate -group data_path -group dlx_rf /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/ENABLE
add wave -noupdate -group data_path -group dlx_rf /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/RD1
add wave -noupdate -group data_path -group dlx_rf /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/RD2
add wave -noupdate -group data_path -group dlx_rf /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/WR
add wave -noupdate -group data_path -group dlx_rf /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/ADD_WR
add wave -noupdate -group data_path -group dlx_rf /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/ADD_RD1
add wave -noupdate -group data_path -group dlx_rf /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/ADD_RD2
add wave -noupdate -group data_path -group dlx_rf /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/DATAIN
add wave -noupdate -group data_path -group dlx_rf /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/WR_RET_ADDR
add wave -noupdate -group data_path -group dlx_rf /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/RET_ADDR_IN
add wave -noupdate -group data_path -group dlx_rf /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/OUT1
add wave -noupdate -group data_path -group dlx_rf /dlx_testbench/DLX_0/data_path_core_0/dlx_rf_0/OUT2
add wave -noupdate -group data_path -group IMM_EXT /dlx_testbench/DLX_0/data_path_core_0/IMMEDIATE_EXT_0/CLK
add wave -noupdate -group data_path -group IMM_EXT /dlx_testbench/DLX_0/data_path_core_0/IMMEDIATE_EXT_0/enable
add wave -noupdate -group data_path -group IMM_EXT /dlx_testbench/DLX_0/data_path_core_0/IMMEDIATE_EXT_0/sign
add wave -noupdate -group data_path -group IMM_EXT /dlx_testbench/DLX_0/data_path_core_0/IMMEDIATE_EXT_0/IN_IMM
add wave -noupdate -group data_path -group IMM_EXT /dlx_testbench/DLX_0/data_path_core_0/IMMEDIATE_EXT_0/OUT_32
add wave -noupdate -group data_path -group ALU_0 -radix decimal /dlx_testbench/DLX_0/data_path_core_0/ALU_0/A
add wave -noupdate -group data_path -group ALU_0 -radix decimal /dlx_testbench/DLX_0/data_path_core_0/ALU_0/B
add wave -noupdate -group data_path -group ALU_0 -radix decimal /dlx_testbench/DLX_0/data_path_core_0/ALU_0/C
add wave -noupdate -group data_path -group ALU_0 /dlx_testbench/DLX_0/data_path_core_0/ALU_0/OVERFLOW
add wave -noupdate -group data_path -group ALU_0 /dlx_testbench/DLX_0/data_path_core_0/ALU_0/ALU_OP
add wave -noupdate -group data_path -group ALU_0 /dlx_testbench/DLX_0/data_path_core_0/ALU_0/adder_input_b
add wave -noupdate -group data_path -group ALU_0 /dlx_testbench/DLX_0/data_path_core_0/ALU_0/adder_out
add wave -noupdate -group data_path -group ALU_0 /dlx_testbench/DLX_0/data_path_core_0/ALU_0/shifter_out
add wave -noupdate -group data_path -group ALU_0 /dlx_testbench/DLX_0/data_path_core_0/ALU_0/int_carry_out
add wave -noupdate -group data_path -group ALU_0 /dlx_testbench/DLX_0/data_path_core_0/ALU_0/nor_int_res
add wave -noupdate -group data_path -group BRANCH /dlx_testbench/DLX_0/data_path_core_0/BRANCH_0/RST
add wave -noupdate -group data_path -group BRANCH /dlx_testbench/DLX_0/data_path_core_0/BRANCH_0/J_ADDR
add wave -noupdate -group data_path -group BRANCH /dlx_testbench/DLX_0/data_path_core_0/BRANCH_0/REG_VALUE
add wave -noupdate -group data_path -group BRANCH /dlx_testbench/DLX_0/data_path_core_0/BRANCH_0/JUMP
add wave -noupdate -group data_path -group BRANCH /dlx_testbench/DLX_0/data_path_core_0/BRANCH_0/COND_JUMP
add wave -noupdate -group data_path -group BRANCH /dlx_testbench/DLX_0/data_path_core_0/BRANCH_0/EQ_ZERO
add wave -noupdate -group data_path -group BRANCH /dlx_testbench/DLX_0/data_path_core_0/BRANCH_0/PC_OUT
add wave -noupdate -group data_path -group BRANCH /dlx_testbench/DLX_0/data_path_core_0/BRANCH_0/BRANCH_REG
add wave -noupdate -group data_path -group BRANCH /dlx_testbench/DLX_0/data_path_core_0/BRANCH_0/BRANCH_TAKEN
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/CLK
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/RESET
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/ENABLE
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/CW
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/PC
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/INSTR
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/RF_WR_ADDR
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/OVERFLOW
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/DRAM_ADDR
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/DRAM_WRITE_DATA
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/DRAM_READ_DATA
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/RF_OUT_A
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/RF_OUT_B
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/B_TO_MUX
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/IMM_TO_MUX
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/RF_IN
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/ALU_IN_A
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/ALU_IN_B
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/ALU_OUT
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/ALU_OUT_LATCHED
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/ALU_OUT_LATCHED_DELAYED
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/LMD_TO_MUX
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/FOR_SEL_IN_A
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/FOR_SEL_IN_B
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/JUMP_ADDR_OUT
add wave -noupdate -group data_path /dlx_testbench/DLX_0/data_path_core_0/JUMP_ADDR_IN
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {22310 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 511
configure wave -valuecolwidth 182
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {17360 ps}
