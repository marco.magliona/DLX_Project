# Run this script from "test_bench" folder using relative paths
vlib work

#Compile memory files
foreach file [glob ./TB_packages/*.vhd] {
	vcom -reportprogress 300 -work work $file
}

foreach file [glob ./TB_romem/*.vhd] {
	vcom -reportprogress 300 -work work $file
}

foreach file [glob ./TB_rwmem/*.vhd] {
	vcom -reportprogress 300 -work work $file
}

#Compile TOP level
# vlog -reportprogress 300 -work work ../synthesis/dlx.v
vcom -reportprogress 300 -work work ../synthesis/dlx.vhdl

#Compile Testbench
vcom -reportprogress 300 -work work TB_TOP_DLX.vhd

#vsim -sdftyp ../synthesis/dlx.sdf -t 10ps -novopt work.dlx_testbench

# Compile Openan library Verilog
vlog -reportprogress 300 -work work ../synthesis/library/NangateOpenCellLibrary.v

vsim -t 10ps -novopt work.dlx_testbench

#add wave *
do wave_dlx.do

run 500 ns
