proc compile_global_files {} {
	vlib work

	vcom -reportprogress 300 -work work ../000-globals.vhd
	vcom -reportprogress 300 -work work ../001-aux_func.vhd
}

proc compile_datapath {} {
	vlib work

	vcom -reportprogress 300 -work work ../01*.vhd
	
	foreach file [glob ../a.b-DataPath.core/a.b.a-ADDER/*.vhd] {
		vcom -reportprogress 300 -work work $file
	}

	foreach file [glob ../a.b-DataPath.core/a.b.e-REGISTER_FILE/*.vhd] {
		vcom -reportprogress 300 -work work $file
	}

	foreach file [glob ../a.b-DataPath.core/*.vhd] {
		vcom -reportprogress 300 -work work $file
	}

# Compile TOP entity
	vcom -reportprogress 300 -work work ../a.b-DataPath.vhd
}

proc compile_dlx_cu {} {
	vlib work

foreach file [glob ../a.a-CU.core/*.vhd] {
	vcom -reportprogress 300 -work work $file
}

# Compile TOP entity
	vcom -reportprogress 300 -work work ../a.a-CU*.vhd
}

proc compile_dlx_others {} {
	vlib work

	vcom -reportprogress 300 -work work ../a.c-IRAM.vhd
}
