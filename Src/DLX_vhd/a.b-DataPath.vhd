library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.myTypes.all;

entity data_path_core is
	generic(BIT_NUM :  natural;
	ADDR_LEN        :  natural;
	ALU_OPC_SIZE    :  natural;
	REG_ADDR_LEN    :  natural);
	port(CLK               :  in std_logic;
	     RESET             :  in std_logic;
	     ENABLE            :  in std_logic;
	     CW                :  in std_logic_vector(CW_SIZE-1 downto 0);
	     JUMP_ADDR_OUT     :  out std_logic_vector(BIT_NUM-1 downto 0);
	     PC                :  in std_logic_vector(BIT_NUM-1 downto 0);
	     INSTR             :  in std_logic_vector(BIT_NUM-1 downto 0);
	     RF_WR_ADDR        :  in std_logic_vector(REG_ADDR_LEN-1 downto 0);
	     JUMP_ADDR_IN      :  in std_logic_vector(BIT_NUM-1 downto 0);
	     OVERFLOW          :  out std_logic;
	     BRANCH_TAKEN      :  out std_logic;
	     -- DRAM interface
	     DRAM_ADDR         :  out std_logic_vector(ADDR_LEN-1 downto 0);
	     DRAM_WRITE_DATA   :  out std_logic_vector(BIT_NUM-1 downto 0);
	     DRAM_READ_DATA    :  in std_logic_vector(BIT_NUM-1 downto 0);
	     DRAM_ENABLE       :  in std_logic;
	     -- FORWARDING signals
	     FOR_SEL_IN_A      : in FOR_MUX_SEL;
	     FOR_SEL_IN_B      : in FOR_MUX_SEL
     );
end entity data_path_core;


architecture struct of data_path_core is

	component register_file_gen is
		generic(BIT_NUM :  natural;
		ADDR_LEN        :  natural);
		port ( CLK      :  IN std_logic;
		       RESET    :  IN std_logic;
		       ENABLE   :  IN std_logic;
		       RD1      :  IN std_logic;
		       RD2      :  IN std_logic;
		       WR       :  IN std_logic;
		       ADD_WR   :  IN std_logic_vector(ADDR_LEN-1 downto 0);
		       ADD_RD1  :  IN std_logic_vector(ADDR_LEN-1 downto 0);
		       ADD_RD2  :  IN std_logic_vector(ADDR_LEN-1 downto 0);
		       DATAIN   :  IN std_logic_vector(BIT_NUM-1 downto 0);
		       OUT1     :  OUT std_logic_vector(BIT_NUM-1 downto 0);
		       OUT2     :  OUT std_logic_vector(BIT_NUM-1 downto 0));
	end component register_file_gen;

	component REG_SIMPLE is
		Generic (BIT_NUM: integer);
		Port (D        : In	std_logic_vector(BIT_NUM-1 downto 0);
		      CLK      : In	std_logic;
		      LATCH_EN : In 	std_logic;
		      RESET    : In	std_logic;
		      Q        : Out	std_logic_vector(BIT_NUM-1 downto 0));
	end component REG_SIMPLE;

	component IMMEDIATE_EXT is
		port (CLK    : in std_logic;
		      enable : in std_logic;
		      sign   : in std_logic;
		      IN_IMM : in std_logic_vector(25 downto 0);
		      OUT_32 : out std_logic_vector(31 downto 0)
	      );
	end component IMMEDIATE_EXT;

	component dlx_rf is
		generic(BIT_NUM : natural;
		ADDR_LEN        : natural);
		port(CLK            : IN std_logic;
		     RESET          : IN std_logic;
		     ENABLE         : IN std_logic;
		     RD1            : IN std_logic;
		     RD2            : IN std_logic;
		     WR             : IN std_logic;
		     ADD_WR         : IN std_logic_vector(ADDR_LEN-1 downto 0);
		     ADD_RD1        : IN std_logic_vector(ADDR_LEN-1 downto 0);
		     ADD_RD2        : IN std_logic_vector(ADDR_LEN-1 downto 0);
		     DATAIN         : IN std_logic_vector(BIT_NUM-1 downto 0);
		     WR_RET_ADDR    : IN std_logic;
		     RET_ADDR_IN    : IN std_logic_vector(BIT_NUM-1 downto 0);
		     OUT1           : OUT std_logic_vector(BIT_NUM-1 downto 0);
		     OUT2           : OUT std_logic_vector(BIT_NUM-1 downto 0)
	     );
	end component dlx_rf;

	component ALU is
		generic (ALU_OP_SIZE : natural := 5);
		port (A              : in std_logic_vector(31 downto 0);
		      B              : in std_logic_vector(31 downto 0);
		      C              : out std_logic_vector(31 downto 0);
		      OVERFLOW       : out std_logic;
		      ALU_OP         : in std_logic_vector(ALU_OP_SIZE-1 downto 0)
	      );
	end component ALU;

	component BRANCH is
		generic (ADDR_SIZE : natural;
		REG_LEN            : natural);
		port(
		     RST       : in std_logic;
		     J_ADDR    : in std_logic_vector(ADDR_SIZE-1 downto 0); -- Destination address if branch taken
		     REG_VALUE : in std_logic_vector(REG_LEN-1 downto 0); -- Value of the register to check
		     JUMP      : in std_logic; -- 1 if the instruction is a jump
		     COND_JUMP : in std_logic; -- 1 if conditional jump, 0 if jump
		     EQ_ZERO   : in std_logic; -- 1 if BEQZ, 0 if BNEQZ
	             BRANCH_REG: in std_logic; -- 1 if JR, 0 otherwise
		     PC_OUT    : out std_logic_vector(ADDR_SIZE-1 downto 0);
	     	     BRANCH_TAKEN: out std_logic
	     );
	end component BRANCH;

	signal RF_OUT_A, RF_OUT_B, A_TO_MUX, B_TO_MUX, B_TO_DRAM, IMM_TO_MUX, RF_IN, RF_IN_DELAYED, int_NPC: std_logic_vector(BIT_NUM-1 downto 0);
	signal ALU_IN_A, ALU_IN_B, ALU_OUT, ALU_OUT_LATCHED, ALU_OUT_LATCHED_DELAYED: std_logic_vector(BIT_NUM-1 downto 0);
	signal LMD_TO_MUX: std_logic_vector(BIT_NUM-1 downto 0);
	signal int_JUMP_ADDR_DELAYED: std_logic_vector(BIT_NUM-1 downto 0);
	-- FORWARDING signals
	signal int_FOR_MEM : std_logic_vector(BIT_NUM-1 downto 0); -- Forwarding from the memory phase
	signal B_TO_FOR_MUX: std_logic_vector(BIT_NUM-1 downto 0);
begin

	-- Registers instances
	RF_B_DELAYED : REG_SIMPLE
	generic map (
			    BIT_NUM => BIT_NUM )
	port map (
			 D          => B_TO_MUX,
			 CLK        => CLK,
			 LATCH_EN   => CW(POS_CW_REG_B_DELAYED_LATCH_EN),
			 RESET      => RESET,
			 Q          => B_TO_DRAM );

	RF_ALU_OUT : REG_SIMPLE
	generic map (
			    BIT_NUM => BIT_NUM )
	port map (
			 D          => ALU_OUT,
			 CLK        => CLK,
			 LATCH_EN   => CW(POS_CW_REG_ALU_OUT_LATCH_EN),
			 RESET      => RESET,
			 Q          => ALU_OUT_LATCHED );

	RF_JUMP_ADDR_DELAYED : REG_SIMPLE
	generic map (
			    BIT_NUM => BIT_NUM )
	port map (
			 D          => JUMP_ADDR_IN,
			 CLK        => CLK,
			 LATCH_EN   => '1',
			 RESET      => RESET,
			 Q          => int_JUMP_ADDR_DELAYED);

	-- In the case that the ALU OUTPUT needs to be written in the RF this is needed to "jump over" the WB phase and preserve
	-- the output for the next CLK cycle (WB phase)
	RF_ALU_OUT_DELAYED : REG_SIMPLE
	generic map (
			    BIT_NUM => BIT_NUM )
	port map (
			 D          => ALU_OUT_LATCHED,
			 CLK        => CLK,
			 LATCH_EN   => CW(POS_CW_REG_ALU_DEL_LATCH_EN),
			 RESET      => RESET,
			 Q          => ALU_OUT_LATCHED_DELAYED );

	RF_RF_IN_DELAYED : REG_SIMPLE
	generic map (
			    BIT_NUM => BIT_NUM )
	port map (
			 D          => RF_IN,
			 CLK        => CLK,
			 LATCH_EN   => CW(POS_CW_RF_WE),
			 RESET      => RESET,
			 Q          => RF_IN_DELAYED );


	RF_LMD : REG_SIMPLE
	generic map (
			    BIT_NUM => BIT_NUM )
	port map (
			 D          => DRAM_READ_DATA,
			 CLK        => CLK,
			 LATCH_EN   => CW(POS_CW_REG_LMD_LATCH_EN),
			 RESET      => RESET,
			 Q          => LMD_TO_MUX );


	dlx_rf_0 : dlx_rf
	generic map (
			    BIT_NUM     => BIT_NUM,
			    ADDR_LEN    => REG_ADDR_LEN )
	port map (
			 CLK            => CLK,
			 RESET          => RESET,
			 ENABLE         => ENABLE,
			 RD1            => CW(POS_CW_REG_A_LATCH_EN),
			 RD2            => CW(POS_CW_REG_B_LATCH_EN),
			 WR             => CW(POS_CW_RF_WE),
			 ADD_WR         => RF_WR_ADDR,
			 ADD_RD1        => INSTR(25 downto 21), -- RD1 field
			 ADD_RD2        => INSTR(20 downto 16), -- RD2 field
			 DATAIN         => RF_IN,
			 WR_RET_ADDR    => CW(POS_CW_WR_RET_ADDR),
			 RET_ADDR_IN    => JUMP_ADDR_IN,
			 OUT1           => RF_OUT_A,
			 OUT2           => RF_OUT_B
		 );

	-- IMMEDIATE EXTENSION
	IMMEDIATE_EXT_0 : IMMEDIATE_EXT
	port map (
			 CLK    => CLK,
			 enable => CW(POS_CW_REG_IMM_LATCH_EN),
			 sign   => CW(POS_CW_IMM_SIGN),
			 IN_IMM => INSTR(25 downto 0),
			 OUT_32 => IMM_TO_MUX
		 );

	-- ALU instance
	ALU_0 : ALU
	generic map (
			    ALU_OP_SIZE => ALU_OPC_SIZE )
	port map (
			 A              => ALU_IN_A,
			 B              => ALU_IN_B,
			 C              => ALU_OUT,
			 OVERFLOW       => OVERFLOW,
			 ALU_OP         => CW(POS_CW_ALU_OP+ALU_OPC_SIZE-1 downto POS_CW_ALU_OP)
		 );

	-- BRANCH instance
	BRANCH_0 : BRANCH
	generic map (
			    ADDR_SIZE => BIT_NUM,
			    REG_LEN   => BIT_NUM)
	port map (
			 RST          => RESET,
			 J_ADDR       => int_JUMP_ADDR_DELAYED,
			 REG_VALUE    => ALU_IN_A,
			 JUMP         => CW(POS_CW_JUMP_EN),
			 COND_JUMP    => CW(POS_CW_COND_JUMP),
			 EQ_ZERO      => CW(POS_CW_EQ_COND),
	     		 BRANCH_REG   => CW(POS_CW_JUMP_REG),
			 PC_OUT       => JUMP_ADDR_OUT,
			 BRANCH_TAKEN => BRANCH_TAKEN
		 );

	-- MUXES instances
	ALU_IN_B <= IMM_TO_MUX when CW(POS_CW_MUXB_SEL) = '0' else
		    B_TO_MUX when CW(POS_CW_MUXB_SEL) = '1' else
		    (others => '0');

	RF_IN <= ALU_OUT_LATCHED_DELAYED when CW(POS_CW_WB_MUX_SEL) = '0' else
		 LMD_TO_MUX when CW(POS_CW_WB_MUX_SEL) = '1' else
		 (others => '0');

	-- FORWARDING muxes
	int_FOR_MEM <= ALU_OUT_LATCHED_DELAYED when CW(POS_CW_FOR_MUX_MEM) = '0' else
		       LMD_TO_MUX when CW(POS_CW_FOR_MUX_MEM) = '1' else
		       (others => '0');

	ALU_IN_A  <= RF_OUT_A when FOR_SEL_IN_A = FOR_NONE else
		    ALU_OUT_LATCHED when FOR_SEL_IN_A = FOR_EXE else
		    int_FOR_MEM when FOR_SEL_IN_A = FOR_MEM else
		    RF_IN_DELAYED when FOR_SEL_IN_A = FOR_WB;

	B_TO_MUX <= RF_OUT_B when FOR_SEL_IN_B = FOR_NONE else
		    ALU_OUT_LATCHED when FOR_SEL_IN_B = FOR_EXE else
		    int_FOR_MEM when FOR_SEL_IN_B = FOR_MEM else
		    RF_IN_DELAYED when FOR_SEL_IN_B = FOR_WB;

	-- DRAM data interface
	process(RESET, DRAM_ENABLE)
	begin
		if RESET = '0' then
			DRAM_ADDR <= (others => '0');
			DRAM_WRITE_DATA <= (others => '0');
		else
			if rising_edge(DRAM_ENABLE) then
				DRAM_ADDR <= ALU_OUT_LATCHED;
				DRAM_WRITE_DATA <= B_TO_DRAM;
			end if;
		end if;
	end process;
-- DRAM_READ_DATA connected to lmd register, see lmd instance
end architecture;
