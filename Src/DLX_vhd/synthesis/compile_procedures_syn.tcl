proc compile_global_files {} {
	global compiled_files
	define_design_lib WORK -path ./WORK
	analyze -library WORK -format vhdl { ../000-globals.vhd ../001-aux_func.vhd }
	puts $compiled_files "../000-globals.vhd"
        puts $compiled_files "../001-aux_func.vhd"
}

proc compile_datapath {} {
	define_design_lib WORK -path ./WORK
	global compiled_files
	set file_list [list ]

	analyze -library WORK -format vhdl ../01-generic_shifter.vhd
	puts $compiled_files ../01-generic_shifter.vhd

	# Scan files in path and store their names in a list
	foreach file [glob ../a.b-DataPath.core/a.b.a-ADDER/*.vhd] {
		lappend file_list $file
	}

	# Order the list (the name of each file follows the oreder in which they have to be compiled)
	set file_list [lsort $file_list]

	# Analize the files
	foreach file $file_list {
		analyze -library WORK -format vhdl $file
		puts $compiled_files $file
	}

	# Clear the list
	set file_list [list ]

	foreach file [glob ../a.b-DataPath.core/a.b.e-REGISTER_FILE/*.vhd] {
		analyze -library WORK -format vhdl $file
		puts $compiled_files $file
	}

	foreach file [glob ../a.b-DataPath.core/*.vhd] {
		analyze -library WORK -format vhdl $file
		puts $compiled_files $file
	}

	# Compile TOP entity
	analyze -library WORK -format vhdl {../a.b-DataPath.vhd}
	puts $compiled_files "../a.b-DataPath.vhd"
}

proc compile_dlx_cu {} {
	define_design_lib WORK -path ./WORK
	global compiled_files

	foreach file [glob ../a.a-CU.core/*.vhd] {
		analyze -library WORK -format vhdl $file
		puts $compiled_files $file
	}

	analyze -library WORK -format vhdl ../a.a-CU_FSM.vhd
	puts $compiled_files ../a.a-CU_FSM.vhd
}

proc compile_dlx_others {} {
	define_design_lib WORK -path ./WORK
	global compiled_files

	analyze -library WORK -format vhdl {../a-DLX.vhd}
	puts $compiled_files "../a-DLX.vhd"
}
