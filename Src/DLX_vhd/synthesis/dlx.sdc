###################################################################

# Created by write_sdc on Mon Sep 19 17:15:11 2016

###################################################################
set sdc_version 2.0

set_units -time ns -resistance MOhm -capacitance fF -voltage V -current mA
create_clock [get_ports CLK]  -period 1  -waveform {0 0.5}
