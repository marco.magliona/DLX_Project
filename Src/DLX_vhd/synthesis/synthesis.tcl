source ./.synopsys_dc.setup
source ./compile_procedures_syn.tcl

set clk_name CLK
set clk_period 1.0
set clk_waveform [list [format %.2f 0.00] [format %.2f 0.5]]

exec rm compiled_files.log
set compiled_files [open compiled_files.log w]
compile_global_files
compile_datapath
compile_dlx_cu
compile_dlx_others
close $compiled_files

elaborate DLX -architecture DLX_RTL -library WORK -parameters "IR_SIZE = 32, PC_SIZE = 32"
# ungroup -flatten -all

# Start to map FSMs
# FETCH FSM
#set current_design FETCH_IR_SIZE32_REG_ADDR_LEN5_BIT_NUM32_OP_CODE_SIZE6
#create_clock -period $clk_period $clk_name -waveform $clk_waveform
#group -fsm -design_name FETCH_FSM
#
## MEMORY FSM
#set current_design MEMORY_STAGE_SIZE19_REG_ADDR_LEN5
#create_clock -period $clk_period $clk_name -waveform $clk_waveform
#group -fsm -design_name MEMORY_FSM

## Group ALU in one block ignoring its internal hierarchy
set current_design ALU_ALU_OP_SIZE5
ungroup -flatten -all
group -logic -design_name DLX_ALU

set current_design DLX_IR_SIZE32_PC_SIZE32
create_clock -period $clk_period $clk_name -waveform $clk_waveform

# compile -exact_map -incremental_mapping
compile_ultra -no_autoungroup
write -hierarchy -format ddc -output dlx.ddc
write -hierarchy -format vhdl -output dlx.vhdl
write -hierarchy -format verilog -output dlx.v
write_sdf dlx.sdf
write_sdc dlx.sdc
report_power > dlx_power.rpt
report_timing > dlx_timing.rpt
