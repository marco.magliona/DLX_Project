library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package myTypes is
	constant BIT_NUM : natural := 32;
	constant ADDR_LEN : natural := 32;
	constant ALU_OPC_SIZE: natural := 5;
	constant OP_CODE_SIZE: natural := 6;
	constant FUNC_SIZE : natural := 11;
	constant IR_SIZE : natural := 32;
	constant QUEUE_SIZE : natural := 8; -- Size of the instruction queue
	constant REG_ADDR_LEN : natural := 5;
	constant NPC_DELAY: natural := 2; -- How many CLK cycles delay the output of the ALU before it reaches the WB mux
	constant NPC_DELAY_JAL: natural := 1; -- How many CLK cycles delay the NPC before it's sent to the RF

	-- ALU types
	constant ALU_ADD  :  std_logic_vector(ALU_OPC_SIZE-1 downto 0)  :=  "00000" ;
	constant ALU_SUB  :  std_logic_vector(ALU_OPC_SIZE-1 downto 0)  :=  "00001" ;
	constant ALU_AND  :  std_logic_vector(ALU_OPC_SIZE-1 downto 0)  :=  "00010" ;
	constant ALU_OR   :  std_logic_vector(ALU_OPC_SIZE-1 downto 0)  :=  "00100" ;
	constant ALU_XOR  :  std_logic_vector(ALU_OPC_SIZE-1 downto 0)  :=  "00110" ;
	constant ALU_GE   :  std_logic_vector(ALU_OPC_SIZE-1 downto 0)  :=  "01001" ;
	constant ALU_G    :  std_logic_vector(ALU_OPC_SIZE-1 downto 0)  :=  "01011" ;
	constant ALU_LE   :  std_logic_vector(ALU_OPC_SIZE-1 downto 0)  :=  "01101" ;
	constant ALU_L    :  std_logic_vector(ALU_OPC_SIZE-1 downto 0)  :=  "01111" ;
	constant ALU_E    :  std_logic_vector(ALU_OPC_SIZE-1 downto 0)  :=  "10001" ;
	constant ALU_NE   :  std_logic_vector(ALU_OPC_SIZE-1 downto 0)  :=  "11001" ;
	-- ALU types shift/rotate
	constant ALU_SLL   :  std_logic_vector(ALU_OPC_SIZE-1 downto 0)  :=  "11111" ;
	constant ALU_SRL   :  std_logic_vector(ALU_OPC_SIZE-1 downto 0)  :=  "11011" ;
	constant ALU_SRA   :  std_logic_vector(ALU_OPC_SIZE-1 downto 0)  :=  "11010" ;

	-- CW size at each stage (IND stands for individual)
	constant CW_IND_STAGE5_SZ : natural := 2;
	constant CW_IND_STAGE4_SZ : natural := 4;
	constant CW_IND_STAGE3_SZ : natural := 7 + ALU_OPC_SIZE;
	constant CW_IND_STAGE2_SZ : natural := 5;
	constant CW_IND_STAGE1_SZ : natural := 0; -- The fetch phase is completely managed by the CU
	-- Signals size for each stage (considering the following stages) USE only for the flipflops in charge of propagating the signals
	constant CW_STAGE5_SZ : natural := CW_IND_STAGE5_SZ;
	constant CW_STAGE4_SZ : natural := CW_IND_STAGE4_SZ + CW_STAGE5_SZ;
	constant CW_STAGE3_SZ : natural := CW_IND_STAGE3_SZ + CW_STAGE4_SZ;
	constant CW_STAGE2_SZ : natural := CW_IND_STAGE2_SZ+ CW_STAGE3_SZ;
	constant CW_STAGE1_SZ : natural := CW_IND_STAGE1_SZ + CW_STAGE2_SZ;

	constant CW_SIZE : natural := CW_STAGE1_SZ;

	-- Signals positions inside their stage vector
	constant POS_FF_REG_IMM_LATCH_EN      : natural  := 0  ;
	constant POS_FF_IMM_SIGN              : natural  := 1  ;
	constant POS_FF_WR_RET_ADDR           : natural  := 2  ;
	constant POS_FF_REG_A_LATCH_EN        : natural  := 3  ;
	constant POS_FF_REG_B_LATCH_EN        : natural  := 4  ;
	constant POS_FF_MUXB_SEL              : natural  := 0  ;
	constant POS_FF_REG_B_DELAYED_LATCH_EN: natural  := 1  ;
	constant POS_FF_REG_ALU_OUT_LATCH_EN  : natural  := 2  ;
	constant POS_FF_JUMP_EN               : natural  := 3  ;
	constant POS_FF_COND_JUMP             : natural  := 4  ;
	constant POS_FF_EQ_COND               : natural  := 5  ;
	constant POS_FF_JUMP_REG              : natural  := 6  ;
	constant POS_FF_ALU_OP                : natural  := 7  ;
	constant POS_FF_DRAM_WE               : natural  := 0  ;
	constant POS_FF_REG_LMD_LATCH_EN      : natural  := 1  ;
	constant POS_FF_REG_ALU_DEL_LATCH_EN  : natural  := 2  ;
	constant POS_FF_FOR_MUX_MEM           : natural  := 3  ; -- Stands for forwarding mux at memory phase, is always equal to WB_MUX_SEL
	constant POS_FF_WB_MUX_SEL            : natural  := 0  ;
	constant POS_FF_RF_WE                 : natural  := 1  ;

	-- Signal potions inside CW
	constant POS_CW_REG_IMM_LATCH_EN      : natural  := 0  ;
	constant POS_CW_IMM_SIGN              : natural  := 1  ;
	constant POS_CW_WR_RET_ADDR           : natural  := 2  ;
	constant POS_CW_REG_A_LATCH_EN        : natural  := 3  ;
	constant POS_CW_REG_B_LATCH_EN        : natural  := 4  ;
	constant POS_CW_MUXB_SEL              : natural  := 5  ;
	constant POS_CW_REG_B_DELAYED_LATCH_EN: natural  := 6  ;
	constant POS_CW_REG_ALU_OUT_LATCH_EN  : natural  := 7  ;
	constant POS_CW_JUMP_EN               : natural  := 8  ;
	constant POS_CW_COND_JUMP             : natural  := 9  ;
	constant POS_CW_EQ_COND               : natural  := 10 ;
	constant POS_CW_JUMP_REG              : natural  := 11 ;
	constant POS_CW_ALU_OP                : natural  := 12 ;
	constant POS_CW_DRAM_WE               : natural  := POS_CW_ALU_OP + ALU_OPC_SIZE + 0  ;
	constant POS_CW_REG_LMD_LATCH_EN      : natural  := POS_CW_ALU_OP + ALU_OPC_SIZE + 1  ;
	constant POS_CW_REG_ALU_DEL_LATCH_EN  : natural  := POS_CW_ALU_OP + ALU_OPC_SIZE + 2  ;
	constant POS_CW_FOR_MUX_MEM           : natural  := POS_CW_ALU_OP + ALU_OPC_SIZE + 3  ;
	constant POS_CW_WB_MUX_SEL            : natural  := POS_CW_ALU_OP + ALU_OPC_SIZE + 4  ;
	constant POS_CW_RF_WE                 : natural  := POS_CW_ALU_OP + ALU_OPC_SIZE + 5  ;

	--OPCODE costants

	constant R_TYPE_NUM : natural := 16#3d#;
	constant NOT_R_TYPE_NUM : natural := 16#3d#;

	constant OPC_NAT_R_TYPE : integer := 16#00#;
	constant OPC_NAT_add    : integer := 16#20#;
	constant OPC_NAT_addi   : integer := 16#08#;
	constant OPC_NAT_addu   : integer := 16#21#;
	constant OPC_NAT_addui  : integer := 16#09#;
	constant OPC_NAT_and    : integer := 16#24#;
	constant OPC_NAT_andi   : integer := 16#0c#;
	constant OPC_NAT_beqz   : integer := 16#04#;
	constant OPC_NAT_bnez   : integer := 16#05#;
	constant OPC_NAT_j      : integer := 16#02#;
	constant OPC_NAT_jal    : integer := 16#03#;
	constant OPC_NAT_jr     : integer := 16#12#;
	constant OPC_NAT_lw     : integer := 16#23#;
	constant OPC_NAT_nop    : integer := 16#15#;
	constant OPC_NAT_or     : integer := 16#25#;
	constant OPC_NAT_ori    : integer := 16#0d#;
	constant OPC_NAT_seq    : integer := 16#28#;
	constant OPC_NAT_seqi   : integer := 16#18#;
	constant OPC_NAT_sge    : integer := 16#2d#;
	constant OPC_NAT_sgei   : integer := 16#1d#;
	constant OPC_NAT_sgt    : integer := 16#2b#;
	constant OPC_NAT_sgti   : integer := 16#1b#;
	constant OPC_NAT_sle    : integer := 16#2c#;
	constant OPC_NAT_slei   : integer := 16#1c#;
	constant OPC_NAT_sll    : integer := 16#04#;
	constant OPC_NAT_slli   : integer := 16#14#;
	constant OPC_NAT_slt    : integer := 16#2a#;
	constant OPC_NAT_slti   : integer := 16#1a#;
	constant OPC_NAT_sne    : integer := 16#29#;
	constant OPC_NAT_snei   : integer := 16#19#;
	constant OPC_NAT_sra    : integer := 16#07#;
	constant OPC_NAT_srl    : integer := 16#06#;
	constant OPC_NAT_srli   : integer := 16#16#;
	constant OPC_NAT_sub    : integer := 16#22#;
	constant OPC_NAT_subi   : integer := 16#0a#;
	constant OPC_NAT_subu   : integer := 16#23#;
	constant OPC_NAT_subui  : integer := 16#0b#;
	constant OPC_NAT_sw     : integer := 16#2b#;
	constant OPC_NAT_xor    : integer := 16#26#;
	constant OPC_NAT_xori   : integer := 16#0e#;

	subtype OPCODE is std_logic_vector(OP_CODE_SIZE-1 downto 0);
	-- OPCODES
	constant OPC_R_TYPE  :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_R_TYPE, OP_CODE_SIZE));
	constant OPC_add     :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_add   , OP_CODE_SIZE));
	constant OPC_addi    :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_addi  , OP_CODE_SIZE));
	constant OPC_addu    :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_addu  , OP_CODE_SIZE));
	constant OPC_addui   :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_addui , OP_CODE_SIZE));
	constant OPC_and     :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_and   , OP_CODE_SIZE));
	constant OPC_andi    :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_andi  , OP_CODE_SIZE));
	constant OPC_beqz    :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_beqz  , OP_CODE_SIZE));
	constant OPC_bnez    :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_bnez  , OP_CODE_SIZE));
	constant OPC_j       :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_j     , OP_CODE_SIZE));
	constant OPC_jal     :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_jal   , OP_CODE_SIZE));
	constant OPC_jr      :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_jr    , OP_CODE_SIZE));
	constant OPC_lw      :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_lw    , OP_CODE_SIZE));
	constant OPC_nop     :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_nop   , OP_CODE_SIZE));
	constant OPC_or      :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_or    , OP_CODE_SIZE));
	constant OPC_ori     :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_ori   , OP_CODE_SIZE));
	constant OPC_seq     :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_seq   , OP_CODE_SIZE));
	constant OPC_seqi    :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_seqi  , OP_CODE_SIZE));
	constant OPC_sge     :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_sge   , OP_CODE_SIZE));
	constant OPC_sgei    :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_sgei  , OP_CODE_SIZE));
	constant OPC_sgt     :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_sgt   , OP_CODE_SIZE));
	constant OPC_sgti    :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_sgti  , OP_CODE_SIZE));
	constant OPC_sle     :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_sle   , OP_CODE_SIZE));
	constant OPC_slei    :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_slei  , OP_CODE_SIZE));
	constant OPC_sll     :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_sll   , OP_CODE_SIZE));
	constant OPC_slli    :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_slli  , OP_CODE_SIZE));
	constant OPC_slt     :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_slt   , OP_CODE_SIZE));
	constant OPC_slti    :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_slti  , OP_CODE_SIZE));
	constant OPC_sne     :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_sne   , OP_CODE_SIZE));
	constant OPC_snei    :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_snei  , OP_CODE_SIZE));
	constant OPC_sra     :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_sra   , OP_CODE_SIZE));
	constant OPC_srl     :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_srl   , OP_CODE_SIZE));
	constant OPC_srli    :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_srli  , OP_CODE_SIZE));
	constant OPC_sub     :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_sub   , OP_CODE_SIZE));
	constant OPC_subi    :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_subi  , OP_CODE_SIZE));
	constant OPC_subu    :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_subu  , OP_CODE_SIZE));
	constant OPC_subui   :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_subui , OP_CODE_SIZE));
	constant OPC_sw      :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_sw    , OP_CODE_SIZE));
	constant OPC_xor     :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_xor   , OP_CODE_SIZE));
	constant OPC_xori    :  OPCODE := std_logic_vector(to_unsigned(OPC_NAT_xori  , OP_CODE_SIZE));

-- Forwarding constants
	subtype FOR_MUX_SEL is std_logic_vector(1 downto 0);
	constant FOR_NONE : FOR_MUX_SEL := "00";
	constant FOR_EXE : FOR_MUX_SEL := "01";
	constant FOR_MEM : FOR_MUX_SEL := "10";
	constant FOR_WB : FOR_MUX_SEL := "11";


	type RF_ADDR_TYPE is array (natural range <>) of std_logic_vector(REG_ADDR_LEN-1 downto 0);
	type FOR_MUX_SEL_TYPE_ARRAY is array (natural range<>) of FOR_MUX_SEL;
end myTypes;

