library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;

entity register_file_gen is
	generic(BIT_NUM  : natural := 32;
		ADDR_LEN        : natural := 5);
	port ( CLK       : IN std_logic;
	       RESET     : IN std_logic;
	       ENABLE     : IN std_logic;
	       RD1        : IN std_logic;
	       RD2        : IN std_logic;
	       WR         : IN std_logic;
	       ADD_WR     : IN std_logic_vector(ADDR_LEN-1 downto 0);
	       ADD_RD1    : IN std_logic_vector(ADDR_LEN-1 downto 0);
	       ADD_RD2    : IN std_logic_vector(ADDR_LEN-1 downto 0);
	       DATAIN     : IN std_logic_vector(BIT_NUM-1 downto 0);
	       OUT1      : OUT std_logic_vector(BIT_NUM-1 downto 0);
	       OUT2       : OUT std_logic_vector(BIT_NUM-1 downto 0));
end register_file_gen;

architecture A of register_file_gen is

	-- suggested structures
	subtype REG_ADDR is natural range 0 to 2**ADDR_LEN-1; -- using natural type
	type REG_ARRAY is array(REG_ADDR) of std_logic_vector(BIT_NUM-1 downto 0);
	signal REGISTERS : REG_ARRAY;


begin

	write: process(CLK)
	begin
		if(CLK = '1' and CLK'event) then
			if(reset = '0') then
				for i in 2**ADDR_LEN-1 downto 0 loop
					REGISTERS(i) <= (others => '0');
				end loop;
			else
				if(enable = '1') then
					if(WR = '1') then
						REGISTERS(to_integer(unsigned(ADD_WR))) <= DATAIN;
					end if;
				end if;
			end if;
		end if;
	end process;

	read: process(CLK)
	begin
		if(CLK = '1' and CLK'event) then
			if(reset = '0') then
				OUT1 <= (others => '0');
				OUT2 <= (others => '0');
			else
			OUT1 <= (others => 'Z');
			OUT2 <= (others => 'Z');
			if(enable = '1') then
				if(RD1 = '1') then
					if(WR = '1' and ADD_RD1 = ADD_WR) then
						OUT1 <= (others => 'Z');
					else
						OUT1 <= REGISTERS(to_integer(unsigned(ADD_RD1)));
					end if;
				end if;
				if(RD2 = '1') then
					if(WR = '1' and ADD_RD2 = ADD_WR) then
						OUT2 <= (others => 'Z');
					else
						OUT2 <= REGISTERS(to_integer(unsigned(ADD_RD2)));
					end if;
				end if;
			end if;
		end if;
	end if;
	end process;

end A;
