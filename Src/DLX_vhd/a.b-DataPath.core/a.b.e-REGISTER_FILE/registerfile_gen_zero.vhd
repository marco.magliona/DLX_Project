library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;

-- Modified version of the register file with the register at address 0 always at 0
entity register_file_gen_zero is
	generic(BIT_NUM  : natural := 32;
		ADDR_LEN        : natural := 5);
	port ( CLK       : IN std_logic;
	       RESET     : IN std_logic;
	       ENABLE     : IN std_logic;
	       RD1        : IN std_logic;
	       RD2        : IN std_logic;
	       WR         : IN std_logic;
	       WR_RET_ADDR: IN std_logic;
	       RET_ADDR_IN: IN std_logic_vector(BIT_NUM-1 downto 0);
	       ADD_WR     : IN std_logic_vector(ADDR_LEN-1 downto 0);
	       ADD_RD1    : IN std_logic_vector(ADDR_LEN-1 downto 0);
	       ADD_RD2    : IN std_logic_vector(ADDR_LEN-1 downto 0);
	       DATAIN     : IN std_logic_vector(BIT_NUM-1 downto 0);
	       OUT1      : OUT std_logic_vector(BIT_NUM-1 downto 0);
	       OUT2       : OUT std_logic_vector(BIT_NUM-1 downto 0));
end register_file_gen_zero;

architecture A of register_file_gen_zero is

	-- suggested structures
	subtype REG_ADDR is natural range 0 to 2**ADDR_LEN-1; -- using natural type
	type REG_ARRAY is array(REG_ADDR) of std_logic_vector(BIT_NUM-1 downto 0);
	signal REGISTERS : REG_ARRAY;

begin
	process(CLK)
	begin
		if(CLK = '1' and CLK'event) then
			if(reset = '0') then
				OUT1 <= (others => '0');
				OUT2 <= (others => '0');
				for i in 2**ADDR_LEN-1 downto 0 loop
					REGISTERS(i) <= (others => '0');
				end loop;
			else
				if(enable = '1') then
					if(WR = '1') then
						if(ADD_WR /= (ADDR_LEN-1 downto 0 => '0')) then
							REGISTERS(to_integer(unsigned(ADD_WR))) <= DATAIN;
						end if;
					end if;
					if(WR_RET_ADDR = '1') then
						REGISTERS(2**ADDR_LEN-1) <= RET_ADDR_IN;
					end if;
					if(RD1 = '1') then
						OUT1 <= REGISTERS(to_integer(unsigned(ADD_RD1)));
					end if;
					if(RD2 = '1') then
						OUT2 <= REGISTERS(to_integer(unsigned(ADD_RD2)));
					end if;
				end if;
			end if;
		end if;
	end process;
end A;
