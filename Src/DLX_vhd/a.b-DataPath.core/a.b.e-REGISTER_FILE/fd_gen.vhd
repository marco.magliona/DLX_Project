library IEEE;
use IEEE.std_logic_1164.all;

entity REG_SIMPLE is
	Generic (BIT_NUM: integer);
	Port (D:	In	std_logic_vector(BIT_NUM-1 downto 0);
	       CLK:	In	std_logic;
	       LATCH_EN: In std_logic;
	       RESET:	In	std_logic;
	       Q:	Out	std_logic_vector(BIT_NUM-1 downto 0));
end REG_SIMPLE;

architecture beh of REG_SIMPLE is -- flip flop D with syncronous reset
begin

process(CLK)
	begin
		if CLK'event and CLK='1' then -- positive edge triggered:
			if RESET='0' then -- active low reset
				Q <= (others => '0');
			else
				if(LATCH_EN='1') then
					Q <= D; -- input is written on output
				end if;
			end if;
		end if;
	end process;

end beh;
