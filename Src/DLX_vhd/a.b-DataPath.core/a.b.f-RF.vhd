library ieee;
use ieee.std_logic_1164.all;

entity dlx_rf is
	generic(BIT_NUM: natural;
	ADDR_LEN: natural);
	port(CLK : IN std_logic;
	     RESET : IN std_logic;
	     ENABLE  : IN std_logic;
	     RD1 : IN std_logic;
	     RD2 : IN std_logic;
	     WR : IN std_logic;
	     ADD_WR  : IN std_logic_vector(ADDR_LEN-1 downto 0);
	     ADD_RD1 : IN std_logic_vector(ADDR_LEN-1 downto 0);
	     ADD_RD2 : IN std_logic_vector(ADDR_LEN-1 downto 0);
	     DATAIN  : IN std_logic_vector(BIT_NUM-1 downto 0);
	     WR_RET_ADDR: IN std_logic;
	     RET_ADDR_IN: IN std_logic_vector(BIT_NUM-1 downto 0);
	     OUT1 : OUT std_logic_vector(BIT_NUM-1 downto 0);
	     OUT2 : OUT std_logic_vector(BIT_NUM-1 downto 0)
     );
end entity dlx_rf;


architecture struct of dlx_rf is
	component register_file_gen_zero is
		generic(BIT_NUM  : natural;
		ADDR_LEN : natural);
		port ( CLK       : IN std_logic;
		       RESET     : IN std_logic;
		       ENABLE     : IN std_logic;
		       RD1        : IN std_logic;
		       RD2        : IN std_logic;
		       WR         : IN std_logic;
	       	       WR_RET_ADDR: IN std_logic;
	               RET_ADDR_IN: IN std_logic_vector(BIT_NUM-1 downto 0);
		       ADD_WR     : IN std_logic_vector(ADDR_LEN-1 downto 0);
		       ADD_RD1    : IN std_logic_vector(ADDR_LEN-1 downto 0);
		       ADD_RD2    : IN std_logic_vector(ADDR_LEN-1 downto 0);
		       DATAIN     : IN std_logic_vector(BIT_NUM-1 downto 0);
		       OUT1      : OUT std_logic_vector(BIT_NUM-1 downto 0);
		       OUT2       : OUT std_logic_vector(BIT_NUM-1 downto 0));
	end component register_file_gen_zero;

	begin
	register_file : register_file_gen_zero -- The real register file, I hope to substitute this with the windowed version --TODO
	generic map (
			    BIT_NUM   => BIT_NUM,
			    ADDR_LEN  => ADDR_LEN )
	port map (
			 CLK        => CLK,
			 RESET      => RESET,
			 ENABLE      => ENABLE,
			 RD1         => RD1,
			 RD2         => RD2,
			 WR          => WR,
	        	 WR_RET_ADDR => WR_RET_ADDR,
			 RET_ADDR_IN => RET_ADDR_IN,
			 ADD_WR      => ADD_WR,
			 ADD_RD1     => ADD_RD1,
			 ADD_RD2     => ADD_RD2,
			 DATAIN      => DATAIN,
			 OUT1       => OUT1,
			 OUT2        => OUT2 );
end architecture;
