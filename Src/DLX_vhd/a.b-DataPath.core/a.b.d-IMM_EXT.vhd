library ieee;
use ieee.std_logic_1164.all;

entity IMMEDIATE_EXT is
	port (CLK: in std_logic;
	      enable: in std_logic;
	      sign: in std_logic;
	      IN_IMM: in std_logic_vector(25 downto 0);
	      OUT_32: out std_logic_vector(31 downto 0)
      );
end entity IMMEDIATE_EXT;

architecture struct of IMMEDIATE_EXT is
begin
	process(CLK)
	begin
		if(rising_edge(CLK)) then
			if(enable = '1') then
				if(sign = '0') then
					OUT_32 <= (15 downto 0 => IN_IMM(15))&IN_IMM(15 downto 0);
				else
					OUT_32 <= (15 downto 0 => '0')&IN_IMM(15 downto 0);
				end if;
			end if;
		end if;
	end process;
end architecture;
