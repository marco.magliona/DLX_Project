library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.or_reduce;
use work.myTypes.all;

entity ALU is
	generic (ALU_OP_SIZE: natural);
	port (A: in std_logic_vector(31 downto 0);
	      B: in std_logic_vector(31 downto 0);
	      C: out std_logic_vector(31 downto 0);
	      OVERFLOW: out std_logic;
	      ALU_OP: in std_logic_vector(ALU_OP_SIZE-1 downto 0)
      );
end entity ALU;


architecture alu_arch of ALU is

	component p4_adder is
		port(carry_in: in std_logic;
		A,B: in std_logic_vector(31 downto 0);
		C: out std_logic_vector(31 downto 0);
		carry_out: out std_logic
	);
	end  component p4_adder;


	component SHIFTER_GENERIC is
		generic(N: integer);
		port(	A: in std_logic_vector(N-1 downto 0);
		      B: in std_logic_vector(4 downto 0);
		      LOGIC_ARITH: in std_logic;	-- 1 = logic, 0 = arith
		      LEFT_RIGHT: in std_logic;	-- 1 = left, 0 = right
		      SHIFT_ROTATE: in std_logic;	-- 1 = shift, 0 = rotate
		      OUTPUT: out std_logic_vector(N-1 downto 0)
	      );

	end component SHIFTER_GENERIC;

	signal adder_input_b: std_logic_vector(31 downto 0);
	signal adder_out, shifter_out : std_logic_vector(31 downto 0);
	signal int_carry_out: std_logic;
	signal nor_int_res: std_logic;
begin
	adder_input_b <= B when ALU_OP(0)='0' else NOT(B) when ALU_OP(0)='1' else (others => 'Z');

	P4adder: p4_adder port map (carry_in => ALU_OP(0), A => A, B => adder_input_b, carry_out => int_carry_out, C => adder_out);
	shifter: SHIFTER_GENERIC generic map (N => 32) port map (A => A, B => B(4 downto 0), LOGIC_ARITH => ALU_OP(0), LEFT_RIGHT => ALU_OP(2), SHIFT_ROTATE => ALU_OP(3), OUTPUT => shifter_out);
	nor_int_res <= not(or_reduce (adder_out));
	C <= adder_out when ALU_OP=ALU_ADD else -- SUM
	     adder_out when ALU_OP=ALU_SUB else -- SUB
	     A AND B when ALU_OP=ALU_AND  else -- AND
	     A OR B when ALU_OP=ALU_OR  else -- OR
	     A XOR B when ALU_OP=ALU_XOR  else -- XOR

	     (30 downto 0 => '0')&(int_carry_out) when ALU_OP=ALU_GE  else -- A>=B
	     (30 downto 0 => '0')&(not(nor_int_res) AND int_carry_out) when ALU_OP=ALU_G  else -- A>B
	     (30 downto 0 => '0')&(not(int_carry_out) or nor_int_res) when ALU_OP=ALU_LE  else -- A<=B
	     (30 downto 0 => '0')&(not(int_carry_out)) when ALU_OP=ALU_L  else -- A<B
	     (30 downto 0 => '0')&(nor_int_res) when ALU_OP=ALU_E  else -- A=B
	     (30 downto 0 => '0')&(not(nor_int_res)) when ALU_OP=ALU_NE  else -- A/=B
	     shifter_out when (ALU_OP(ALU_OP_SIZE-1) AND ALU_OP(1)) = '1' else -- SHIFT/ROTATE OPCODE in the form 1xx1x
	     (others => '0');

	OVERFLOW <= int_carry_out when ALU_OP = (ALU_OP'left downto 0 => '0') else '0';

end architecture;
