library ieee; 
use ieee.std_logic_1164.all;

entity prop_gen is
port (Gik,Pik,Gk_1j,Pk_1j: in std_logic;
      Gij, Pij: out std_logic);
end entity;

architecture beh of prop_gen is
begin
Gij <= Gik or (Pik and Gk_1j);
Pij <= Pik and Pk_1j;
end architecture;

