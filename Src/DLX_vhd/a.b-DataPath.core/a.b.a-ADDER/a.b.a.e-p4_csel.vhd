library ieee;
use ieee.std_logic_1164.all;

entity p4_csel is
port(carry_in: IN std_logic_vector(8 downto 0);
     A: in std_logic_vector(31 downto 0);
     B: IN std_logic_vector(31 downto 0);
     RES: OUT std_logic_vector(31 downto 0)
     );
end entity p4_csel;

architecture struct of p4_csel is

component RCA is 
	generic ( BIT_NUM : natural);
	Port (	A:	In	std_logic_vector(BIT_NUM-1 downto 0);
		B:	In	std_logic_vector(BIT_NUM-1 downto 0);
		Ci:	In	std_logic;
		S:	Out	std_logic_vector(BIT_NUM-1 downto 0);
		Co:	Out	std_logic);
end component RCA; 

type RCA_out_type is array(8 downto 0) of std_logic_vector((4*2)-1 downto 0);

signal RCA_out: RCA_out_type;

begin

rca_gen: for i in 7 downto 0 generate
	-- RCA 4 bits connect here carry in '0' -- mux 0
	RCA_zero: RCA generic map (BIT_NUM => 4) port map(A => A(4*(i+1)-1 downto 4*i), B => B(4*(i+1)-1 downto 4*i), S => RCA_out(i)(3 downto 0), CI => '0');
	-- RCA 4 bits connect here carry in '1' -- mux 1
	RCA_one: RCA generic map (BIT_NUM => 4) port map(A => A(4*(i+1)-1 downto 4*i), B => B(4*(i+1)-1 downto 4*i), S => RCA_out(i)(7 downto 4), CI => '1');
	RES(4*(i+1)-1 downto 4*i) <= RCA_out(i)(3 downto 0) when carry_in(i) = '0' else RCA_out(i)(7 downto 4); 	
end generate;

end architecture;
