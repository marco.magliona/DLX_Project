library ieee;
use ieee.std_logic_1164.all;

entity s_tree is
port(A,B: in std_logic_vector(31 downto 0);
     Cin: in std_logic;
     C: out std_logic_vector(8 downto 0));
end s_tree;

architecture struct of s_tree is

component prop_gen is
port (Gik,Pik,Gk_1j,Pk_1j: in std_logic;
      Gij, Pij: out std_logic);
end component;

type SignalV is array (32 downto 0) of std_logic_vector(32 downto 0);
signal GC,PC: SignalV;

begin

-- PG network
p_g_gen: for i in 31 downto 0 generate -- PG network
	PC(i+1)(i+1) <= A(i) XOR B(i);
	GC(i+1)(i+1) <= A(i) AND B(i);
end generate;

G0: prop_gen port map(Gik => B(0), Pik => A(0), Gk_1j => Cin, Pk_1j => '0',
		      Gij => GC(1)(0));
-- END PG network

-- Start PG blocks

-- The block at row "ROW" has j = 2**(ROW+1)+1, and i=2**(ROW+2)
-- Each row has 2**(4-ROW)-1 PG blocks

pg_row: for row in 0 to 2 generate -- j = (2**(row+1)+1) + num * 2**(row+1)
				   -- i = j + distance -1 where distance is 2**(row+1) => i =  (2**(row+1)+1) + (num+1) * 2**(row+1) - 1
				   -- k = j + distance/2 => (2**(row+1)+1) + num * 2**(row+1) + 2**row
	pg_element: for num in 0 to 2**(4-row)-2 generate
		pg_n: prop_gen port map(Gik => GC((2**(row+1)+1) + (num+1) * 2**(row+1) - 1)((2**(row+1)+1) + num * 2**(row+1) + 2**row),
				        Pik => PC((2**(row+1)+1) + (num+1) * 2**(row+1) - 1)((2**(row+1)+1) + num * 2**(row+1) + 2**row),
				        Gk_1j => GC((2**(row+1)+1) + num * 2**(row+1) + 2**row -1)((2**(row+1)+1) + num * 2**(row+1)),
					Pk_1j => PC((2**(row+1)+1) + num * 2**(row+1) + 2**row -1)((2**(row+1)+1) + num * 2**(row+1)),
			      		Gij => GC((2**(row+1)+1) + (num+1) * 2**(row+1) - 1)((2**(row+1)+1) + num * 2**(row+1)),
					Pij => PC((2**(row+1)+1) + (num+1) * 2**(row+1) - 1)((2**(row+1)+1) + num * 2**(row+1)));
	end generate pg_element;
end generate pg_row;

-- Last two blocks => row 3

pg_last_1: prop_gen port map(Gik => GC(28)(25), Pik => PC(28)(25), Gk_1j => GC(24)(17), Pk_1j => PC(24)(17),
			     Gij => GC(28)(17), Pij => PC(28)(17));

pg_last_2: prop_gen port map(Gik => GC(32)(25), Pik => PC(32)(25), Gk_1j => GC(24)(17), Pk_1j => PC(24)(17),
			     Gij => GC(32)(17), Pij => PC(32)(17));

-- End PG blocks

--Start G blocks

--First row
gn1: prop_gen port map(Gik => GC(2)(2), Pik => PC(2)(2), Gk_1j => GC(1)(0), Pk_1j => '0',
			    	       Gij => GC(2)(0));

--Second row
gn2: prop_gen port map(Gik => GC(4)(3), Pik => PC(4)(3), Gk_1j => GC(2)(0), Pk_1j => '0',
			    	       Gij => GC(4)(0));

--Third row
gn3: prop_gen port map(Gik => GC(8)(5), Pik => PC(8)(5), Gk_1j => GC(4)(0), Pk_1j => '0',
			    	       Gij => GC(8)(0));

--Forth row
gn4: prop_gen port map(Gik => GC(12)(9), Pik => PC(12)(9), Gk_1j => GC(8)(0), Pk_1j => '0',
			    	       Gij => GC(12)(0));
gn4_1: prop_gen port map(Gik => GC(16)(9), Pik => PC(16)(9), Gk_1j => GC(8)(0), Pk_1j => '0',
			    	       Gij => GC(16)(0));

--Fifth row
gn5_1: prop_gen port map(Gik => GC(20)(17), Pik => PC(20)(17), Gk_1j => GC(16)(0), Pk_1j => '0',
			    	       Gij => GC(20)(0));
gn5_2: prop_gen port map(Gik => GC(24)(17), Pik => PC(24)(17), Gk_1j => GC(16)(0), Pk_1j => '0',
			    	       Gij => GC(24)(0));
gn5_3: prop_gen port map(Gik => GC(28)(17), Pik => PC(28)(17), Gk_1j => GC(16)(0), Pk_1j => '0',
			    	       Gij => GC(28)(0));
gn5_4: prop_gen port map(Gik => GC(32)(17), Pik => PC(32)(17), Gk_1j => GC(16)(0), Pk_1j => '0',
			    	       Gij => GC(32)(0));
--End G blocks

carry_out: for i in 8 downto 1 generate
	C(i) <= GC(i*4)(0);
end generate;

C(0) <= Cin;

end struct;
