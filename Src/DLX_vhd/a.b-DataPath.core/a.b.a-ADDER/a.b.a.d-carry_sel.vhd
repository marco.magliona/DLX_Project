library ieee;
use ieee.std_logic_1164.all;

entity csel is
generic(BIT_NUM: natural);
port(CI: IN std_logic;
     A: in std_logic_vector(BIT_NUM-1 downto 0);
     B: IN std_logic_vector(BIT_NUM-1 downto 0);
     RES: OUT std_logic_vector(BIT_NUM-1 downto 0)
     );
end entity csel;
 
architecture struct of csel is
component RCA is 
	generic (BIT_NUM : natural);
	Port (	A:	In	std_logic_vector(BIT_NUM-1 downto 0);
		B:	In	std_logic_vector(BIT_NUM-1 downto 0);
		Ci:	In	std_logic;
		S:	Out	std_logic_vector(BIT_NUM-1 downto 0);
		Co:	Out	std_logic);
end component RCA; 

signal res_zero, res_one: std_logic_vector(BIT_NUM-1 downto 0);

begin
-- RCA 4 bits connect here carry in '0' -- mux 0
	RCA_zero: RCA generic map (BIT_NUM => BIT_NUM) port map(A => A, B => B, S => res_zero, CI => '0');
	-- RCA 4 bits connect here carry in '1' -- mux 1
	RCA_one: RCA generic map (BIT_NUM => BIT_NUM) port map(A => A, B => B, S => res_one, CI => '1');
	RES <= res_zero when CI = '0' else res_one;

end architecture;
