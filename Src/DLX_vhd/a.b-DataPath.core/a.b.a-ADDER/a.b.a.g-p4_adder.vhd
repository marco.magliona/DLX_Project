library ieee;
use ieee.std_logic_1164.all;

entity p4_adder is
port(carry_in: in std_logic;
     A,B: in std_logic_vector(31 downto 0);
     C: out std_logic_vector(31 downto 0);
     carry_out: out std_logic
     );
end entity p4_adder;

architecture struct of p4_adder is
component p4_csel is
port(carry_in: IN std_logic_vector(8 downto 0);
     A: in std_logic_vector(31 downto 0);
     B: IN std_logic_vector(31 downto 0);
     RES: OUT std_logic_vector(31 downto 0)
     );
end component p4_csel;

component s_tree is
port(A,B: in std_logic_vector(31 downto 0);
     Cin: in std_logic;
     C: out std_logic_vector(8 downto 0));
end component s_tree;

signal c_to_sum_gen: std_logic_vector(8 downto 0);

begin

s_tree_ist : s_tree port map(A=> A, B=> B, Cin => carry_in, C => c_to_sum_gen);
carry_sel_ist : p4_csel	port map (carry_in => c_to_sum_gen, A=> A, B=>B, RES => C);

carry_out <= c_to_sum_gen(8);

end architecture;
