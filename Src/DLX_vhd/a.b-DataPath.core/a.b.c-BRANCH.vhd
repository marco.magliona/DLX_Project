library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BRANCH is
	generic (ADDR_SIZE: natural;
	REG_LEN : natural);
	port(
		    RST: in std_logic;
		    J_ADDR: in std_logic_vector(ADDR_SIZE-1 downto 0); -- Destination address if branch taken
		    REG_VALUE: in std_logic_vector(REG_LEN-1 downto 0); -- Value of the register to check
		    JUMP:in std_logic; -- 1 if the instruction is a jump
		    COND_JUMP: in std_logic; -- 1 if conditional jump, 0 if jump
		    EQ_ZERO: in std_logic; -- 1 if BEQZ, 0 if BNEQZ
		    BRANCH_REG: in std_logic; -- 1 if JR, 0 otherwise
		    PC_OUT: out std_logic_vector(ADDR_SIZE-1 downto 0);
		    BRANCH_TAKEN: out std_logic
	    );
end entity BRANCH;

architecture branch_arch of BRANCH is
begin
	process(BRANCH_REG, RST, JUMP, COND_JUMP, J_ADDR, EQ_ZERO, REG_VALUE)
	begin
		if RST = '0' then
			BRANCH_TAKEN <= '0';
			PC_OUT <= (others => '0');
		else
			BRANCH_TAKEN <= '0';
			if(COND_JUMP = '0') then -- jump instruction (jump in anycase)
				PC_OUT <= J_ADDR;
				if(BRANCH_REG = '1') then
					PC_OUT <= REG_VALUE;
				end if;
				if(JUMP='1') then
					BRANCH_TAKEN <= '1';
				end if;
			else -- conditional jump
				if(EQ_ZERO = '0') then -- BNEQZ
					if(unsigned(REG_VALUE) /= 0) then
						PC_OUT <= J_ADDR;
						if(JUMP = '1') then
							BRANCH_TAKEN <= '1';
						end if;
					end if;
				else -- BEQZ
					if(unsigned(REG_VALUE) = 0) then
						PC_OUT <= J_ADDR;
						if(JUMP = '1') then
							BRANCH_TAKEN <= '1';
						end if;
					end if;
				end if;
			end if;
		end if;
	end process;

end architecture branch_arch;
