library ieee;
use ieee.std_logic_1164.all;
use work.myTypes.all;
use work.utils.all;
use ieee.numeric_std.all;
--use work.all;

entity dlx_cu is
	port (
		     CLK            : in  std_logic;
		     Rst            : in  std_logic;
		     IR_IN          : in  std_logic_vector(2*IR_SIZE - 1 downto 0);
		     JUMP_ADDR_IN   : in  std_logic_vector(BIT_NUM-1 downto 0);
		     IR_OUT         : out std_logic_vector(IR_SIZE -1 downto 0);
		     PC_OUT         : out std_logic_vector(BIT_NUM-1 downto 0);
		     RF_WR_ADDR     : out std_logic_vector(REG_ADDR_LEN-1 downto 0);
		     JUMP_ADDR_OUT  : out std_logic_vector(BIT_NUM-1 downto 0);
		     OVERFLOW       : in  std_logic;
		     BRANCH_TAKEN   : in  std_logic;
		     CW             : out std_logic_vector(CW_SIZE-1 downto 0);
		     IRAM_ENABLE    : out std_logic;
		     IRAM_READY     : in  std_logic;
		     IRAM_ADDRESS   : out std_logic_vector(BIT_NUM-1 downto 0);
		     DRAM_READY     : in  std_logic;
		     DRAM_ENABLE    : out std_logic;
	             -- FORWARDING signals
		     FOR_SEL_IN_A   : out FOR_MUX_SEL;
	     	     FOR_SEL_IN_B   : out FOR_MUX_SEL
	     );
end dlx_cu;

architecture dlx_cu_fsm of dlx_cu is

	component INSTR_QUEUE is
		generic (IR_SIZE    : natural := 32  ;
			 QUEUE_SIZE : natural := 8   ;
			 BIT_NUM    : natural := 32) ;
		port (
			     CLK          : in std_logic;
			     RST          : in std_logic;
		             BRANCH_TAKEN : in std_logic;
			     IRAM_ENABLE  : out std_logic;
			     IRAM_READY   : in std_logic;
			     IRAM_ADDRESS : out std_logic_vector(BIT_NUM-1 downto 0);
			     -- When RST is HIGH, get the PC value and start reading data from IRAM at address PC
			     PC_IN        : in std_logic_vector(BIT_NUM-1 downto 0);
			     IR_IN        : in std_logic_vector(2*IR_SIZE - 1 downto 0);
			     IR_OUT       : out std_logic_vector(2*IR_SIZE -1 downto 0);
			     PC_OUT       : out std_logic_vector(BIT_NUM-1 downto 0);
			     -- The MSB of JUMP_ADDR is the write enable signal for the RF to write the return address when JAL
			     JUMP_ADDR    : out std_logic_vector(2*BIT_NUM+1 downto 0);
			     NEXT_INSTR   : in std_logic;
			     IR_OUT_READY : out std_logic
		     );
	end component INSTR_QUEUE;

	component FOR_UNIT is
		port (
			     CLK: in std_logic;
			     RST: in std_logic;
			     -- FROM DECODE (position 0), EXECUTE, MEMORY, WRITE BACK stages
			     RD_IN: in RF_ADDR_TYPE(3 downto 0);
			     RD_REQUIRED: in RF_ADDR_TYPE(1 downto 0);
			     MUX_SEL: out FOR_MUX_SEL_TYPE_ARRAY(1 downto 0)
		     );
	end component FOR_UNIT;

	-- Components declaration for stages' FSMs
	component FETCH is
		generic (IR_SIZE: natural;
		REG_ADDR_LEN: natural;
		BIT_NUM: natural;
		OP_CODE_SIZE : natural
	);
	port ( CLK: in std_logic;
	       RST : in std_logic;
	       BRANCH_TAKEN : in std_logic;
	       MEM_WAIT: out std_logic;
	       DRAM_READY: in std_logic;
	       INSTR_IN : in std_logic_vector(2*IR_SIZE -1 downto 0);
	       INSTR_IN_READY: in std_logic;
       	       PC_IN : in std_logic_vector(BIT_NUM-1 downto 0);
	       NEXT_INSTR : out std_logic;
	       NEXT_STAGE_GO : out std_logic;
	       INSTR_OUT : out std_logic_vector(IR_SIZE -1 downto 0);
               PC_OUT : out std_logic_vector(BIT_NUM-1 downto 0);
	       JUMP_ADDR_IN: in std_logic_vector(2*BIT_NUM+1 downto 0);
	       JUMP_ADDR_OUT: out std_logic_vector(BIT_NUM downto 0)
       );
	end component FETCH;

	component DECODE is
		generic (IR_SIZE: natural;
		REG_ADDR_LEN: natural;
		OP_CODE_SIZE: natural;
		STAGE_SIZE: natural;
		CW_SIZE : natural;
		BIT_NUM: natural
	);
	port ( CLK: in std_logic;
	       RST : in std_logic;
	       BRANCH_TAKEN : in std_logic;
	       MEM_WAIT: in std_logic;
	       INSTR_IN : in std_logic_vector(IR_SIZE -1 downto 0);
	       PREV_STAGE_GO: in std_logic; -- HIGH if previous stage has finished and this needs to start
	       NEXT_STAGE_GO : out std_logic;
	       INSTR_OUT : out std_logic_vector(IR_SIZE -1 downto 0);
	       JUMP_ADDR_IN : in std_logic_vector(BIT_NUM downto 0);
	       JUMP_ADDR_OUT : out std_logic_vector(BIT_NUM-1 downto 0);
	       STAGE_CW_OUT: out std_logic_vector(STAGE_SIZE-1 downto 0);
       	       RD_OUT: out std_logic_vector(REG_ADDR_LEN-1 downto 0);
	-- Input register addresses for forwarding unit
	       REG_A_ADDR: out std_logic_vector(REG_ADDR_LEN-1 downto 0);
	       REG_B_ADDR: out std_logic_vector(REG_ADDR_LEN-1 downto 0)
       );
	end component DECODE;

	component EXECUTE is
		generic (STAGE_SIZE: natural;
		         REG_ADDR_LEN: natural
			);
		port ( CLK: in std_logic;
		       RST : in std_logic;
		       BRANCH_TAKEN : in std_logic;
		       MEM_WAIT: in std_logic;
		       STAGE_CW_IN: in std_logic_vector(STAGE_SIZE-1 downto 0);
       		       RD_IN : in std_logic_vector(REG_ADDR_LEN-1 downto 0);
		       PREV_STAGE_GO: in std_logic; -- HIGH if previous stage has finished and this needs to start
		       NEXT_STAGE_GO : out std_logic;
		       STAGE_CW_OUT: out std_logic_vector(STAGE_SIZE-1 downto 0);
       		       RD_OUT : out std_logic_vector(REG_ADDR_LEN-1 downto 0)
	       );
	end component EXECUTE;

	component MEMORY is
		generic (STAGE_SIZE: natural;
		 	 REG_ADDR_LEN : natural
			);
		port ( CLK: in std_logic;
		       RST : in std_logic;
		       BRANCH_TAKEN : in std_logic;
		       MEM_WAIT: in std_logic;
		       STAGE_CW_IN: in std_logic_vector(STAGE_SIZE-1 downto 0);
       		       RD_IN : in std_logic_vector(REG_ADDR_LEN-1 downto 0);
		       PREV_STAGE_GO: in std_logic; -- HIGH if previous stage has finished and this needs to start
		       NEXT_STAGE_GO : out std_logic;
		       STAGE_CW_OUT: out std_logic_vector(STAGE_SIZE-1 downto 0);
       		       RD_OUT : out std_logic_vector(REG_ADDR_LEN-1 downto 0);
	       --Memory interface
		       DRAM_ENABLE: out std_logic;
		       DRAM_READY: in std_logic
	       );
	end component MEMORY;

	component WRITE_BACK is
		generic (STAGE_SIZE: natural;
			 REG_ADDR_LEN: natural
			);
		port ( CLK: in std_logic;
		       RST : in std_logic;
		       BRANCH_TAKEN : in std_logic;
		       MEM_WAIT: in std_logic;
		       STAGE_CW_IN: in std_logic_vector(STAGE_SIZE-1 downto 0);
                       RD_IN : in std_logic_vector(REG_ADDR_LEN-1 downto 0);
		       PREV_STAGE_GO: in std_logic; -- HIGH if previous stage has finished and this needs to start
		       STAGE_CW_OUT: out std_logic_vector(STAGE_SIZE-1 downto 0);
       		       RD_OUT : out std_logic_vector(REG_ADDR_LEN-1 downto 0)
	       );
	end component WRITE_BACK;

	signal int_out_cw      : std_logic_vector(CW_SIZE - 1 downto 0); -- full control word to output
	signal next_stage_go_in, next_stage_go_out   : std_logic_vector(3 downto 0);
	signal int_MEM_WAIT : std_logic;

	-- Signals for the piped output signals
	-- The destination register address should be stored and used at the wb phase
	signal stage2_in_s : std_logic_vector(CW_STAGE2_SZ-1 downto 0);
	signal stage3_in_s : std_logic_vector(CW_STAGE3_SZ-1 downto 0);
	signal stage4_in_s : std_logic_vector(CW_STAGE4_SZ-1 downto 0);
	signal stage5_in_s : std_logic_vector(CW_STAGE5_SZ-1 downto 0);

	signal stage1_out_s : std_logic_vector(CW_STAGE1_SZ-1 downto 0);
	signal stage2_out_s : std_logic_vector(CW_STAGE2_SZ-1 downto 0);
	signal stage3_out_s : std_logic_vector(CW_STAGE3_SZ-1 downto 0);
	signal stage4_out_s : std_logic_vector(CW_STAGE4_SZ-1 downto 0);

	-- Store the destination register address at each stage
	signal RD_ADDR_ARRAY: RF_ADDR_TYPE(3 downto 0);

	signal next_instr_out, next_instr_in: std_logic;
	signal ir_out_ready: std_logic;
	signal int_IR_OUT: std_logic_vector(IR_SIZE-1 downto 0);
	signal int_instr : std_logic_vector(2*IR_SIZE-1 downto 0);
	signal instr_queue_PC_IN: std_logic_vector(BIT_NUM-1 downto 0);
	signal instr_queue_PC_OUT: std_logic_vector(BIT_NUM-1 downto 0);

	signal int_rf_jal_to_fetch : std_logic_vector(2*BIT_NUM+1 downto 0);
	signal int_rf_jal_to_decode : std_logic_vector(BIT_NUM downto 0);

	constant F : natural := 0;
	constant D : natural := 1;
	constant E : natural := 2;
	constant M : natural := 3;
	constant W : natural := 4;

-- FORWARDING signals
signal FOR_ADDR_FROM_STAGES: RF_ADDR_TYPE(3 downto 0);
signal FOR_ADDR_REQUIRED_BY_EXE: RF_ADDR_TYPE(1 downto 0);
signal FOR_MUX_SEL_SIGNALS: FOR_MUX_SEL_TYPE_ARRAY(1 downto 0);
signal REG_A_ADDR, REG_B_ADDR: std_logic_vector(REG_ADDR_LEN-1 downto 0);

begin  -- dlx_cu_rtl

	next_instr_in <= next_instr_out when Rst = '1' else '0';

	NEXT_STAGE_GO_RST: for i in 3 downto 0 generate
		next_stage_go_in(i) <= next_stage_go_out(i) when Rst = '1' else '0';
	end generate;

	INSTR_QUEUE_0 : INSTR_QUEUE
	generic map (
			    IR_SIZE  => IR_SIZE,
			    QUEUE_SIZE => QUEUE_SIZE,
			    BIT_NUM  => BIT_NUM )
	port map (
			 CLK => CLK,
			 RST => RST,
			 BRANCH_TAKEN => BRANCH_TAKEN,
			 IRAM_ENABLE  => IRAM_ENABLE,
			 IRAM_READY  => IRAM_READY,
			 IRAM_ADDRESS  => IRAM_ADDRESS,
			 PC_IN  => instr_queue_PC_IN,
			 IR_IN  => IR_IN,
			 IR_OUT  => int_instr,
			 PC_OUT  => instr_queue_PC_OUT,
			 JUMP_ADDR  => int_rf_jal_to_fetch,
			 NEXT_INSTR => next_instr_in,
			 IR_OUT_READY  => ir_out_ready
		 );

	instr_queue_PC_IN <= (others => '0') when RST = '0' else JUMP_ADDR_IN;

	-- FORWARDING signals and unit
	FOR_ADDR_FROM_STAGES(0) <= RD_ADDR_ARRAY(0);
	FOR_ADDR_FROM_STAGES(1) <= RD_ADDR_ARRAY(1);
	FOR_ADDR_FROM_STAGES(2) <= RD_ADDR_ARRAY(2);
	FOR_ADDR_FROM_STAGES(3) <= RD_ADDR_ARRAY(3);

	FOR_SEL_IN_A <= FOR_MUX_SEL_SIGNALS(0);
	FOR_SEL_IN_B <= FOR_MUX_SEL_SIGNALS(1);

	FOR_ADDR_REQUIRED_BY_EXE(0) <= REG_A_ADDR;
	FOR_ADDR_REQUIRED_BY_EXE(1) <= REG_B_ADDR;

	FOR_UNIT_0 : FOR_UNIT
	port map (
			 CLK => CLK,
			 RST => RST,
			 RD_IN => FOR_ADDR_FROM_STAGES,
			 RD_REQUIRED => FOR_ADDR_REQUIRED_BY_EXE,
			 MUX_SEL => FOR_MUX_SEL_SIGNALS
		 );

	-- FETCH signals
	-- All internal

	-- DECODE signals
	stage2_in_s <= stage1_out_s(CW_SIZE-1 downto CW_STAGE1_SZ-CW_STAGE2_SZ) when Rst = '1' else (others => '0');
	int_out_cw(POS_CW_REG_A_LATCH_EN  )  <=  stage2_in_s(POS_FF_REG_A_LATCH_EN  );
	int_out_cw(POS_CW_REG_B_LATCH_EN  )  <=  stage2_in_s(POS_FF_REG_B_LATCH_EN  );
	int_out_cw(POS_CW_REG_IMM_LATCH_EN)  <=  stage2_in_s(POS_FF_REG_IMM_LATCH_EN);
	int_out_cw(POS_CW_IMM_SIGN        )  <=  stage2_in_s(POS_FF_IMM_SIGN        );
	int_out_cw(POS_CW_WR_RET_ADDR     )  <=  stage2_in_s(POS_FF_WR_RET_ADDR     );

	-- EXECUTE signals
	stage3_in_s <= stage2_out_s(CW_SIZE-CW_IND_STAGE1_SZ-1 downto CW_STAGE2_SZ-CW_STAGE3_SZ) when Rst = '1' else (others => '0');
	int_out_cw(POS_CW_MUXB_SEL               ) <= stage3_in_s(POS_FF_MUXB_SEL               ) ;
	int_out_cw(POS_CW_REG_ALU_OUT_LATCH_EN   ) <= stage3_in_s(POS_FF_REG_ALU_OUT_LATCH_EN   ) ;
	int_out_cw(POS_CW_REG_B_DELAYED_LATCH_EN ) <= stage3_in_s(POS_FF_REG_B_DELAYED_LATCH_EN ) ;
	int_out_cw(POS_CW_JUMP_EN )                <= stage3_in_s(POS_FF_JUMP_EN ) ;
	int_out_cw(POS_CW_COND_JUMP)               <= stage3_in_s(POS_FF_COND_JUMP) ;
	int_out_cw(POS_CW_EQ_COND  )               <= stage3_in_s(POS_FF_EQ_COND  ) ;
	int_out_cw(POS_CW_JUMP_REG  )              <= stage3_in_s(POS_FF_JUMP_REG  ) ;
	int_out_cw(POS_CW_ALU_OP+ALU_OPC_SIZE-1 downto POS_CW_ALU_OP )  <=  stage3_in_s(POS_FF_ALU_OP+ALU_OPC_SIZE-1 downto POS_FF_ALU_OP) ;

	-- MEMORY signals
	stage4_in_s <= stage3_out_s(CW_SIZE-CW_IND_STAGE1_SZ-CW_IND_STAGE2_SZ-1 downto CW_STAGE3_SZ-CW_STAGE4_SZ) when Rst = '1' else (others => '0');
	int_out_cw(POS_CW_DRAM_WE ) <= stage4_in_s(POS_FF_DRAM_WE ) ;
	-- Quick and dirty solution for the strange timing of the dram, it just works, nothing more to show here
	int_out_cw(POS_CW_REG_LMD_LATCH_EN) <= stage4_in_s(POS_FF_REG_LMD_LATCH_EN) AND DRAM_READY;
	int_out_cw(POS_CW_REG_ALU_DEL_LATCH_EN) <= stage4_in_s(POS_FF_REG_ALU_DEL_LATCH_EN);
	int_out_cw(POS_CW_FOR_MUX_MEM) <= stage4_in_s(POS_FF_FOR_MUX_MEM);

	-- WRITE BACK signals
	stage5_in_s <= stage4_out_s(CW_SIZE-CW_IND_STAGE1_SZ-CW_IND_STAGE2_SZ-CW_IND_STAGE3_SZ-1 downto CW_STAGE4_SZ-CW_STAGE5_SZ) when Rst = '1' else (others => '0');
	int_out_cw(POS_CW_WB_MUX_SEL  ) <= stage5_in_s(POS_FF_WB_MUX_SEL  ) ;
	int_out_cw(POS_CW_RF_WE       ) <= stage5_in_s(POS_FF_RF_WE       ) ;
	RF_WR_ADDR <= RD_ADDR_ARRAY(3) when Rst = '1' else (others => '0');

	CW <= (others => '0') when RST = '0' else int_out_cw;

		-- Instance of the stages entities
	FETCH_0 : FETCH
	generic map (
			    IR_SIZE => IR_SIZE,
			    REG_ADDR_LEN => REG_ADDR_LEN,
			    BIT_NUM => BIT_NUM,
			    OP_CODE_SIZE => OP_CODE_SIZE
		    )
	port map (
			 CLK => CLK,
			 RST  => RST,
			 BRANCH_TAKEN  => BRANCH_TAKEN,
			 MEM_WAIT => int_MEM_WAIT,
			 DRAM_READY => DRAM_READY,
			 INSTR_IN  => int_instr,
			 INSTR_IN_READY => ir_out_ready,
			 PC_IN => instr_queue_PC_OUT,
			 NEXT_INSTR  => next_instr_out,
			 NEXT_STAGE_GO  => next_stage_go_out(F),
			 INSTR_OUT  => int_IR_OUT,
			 PC_OUT => PC_OUT,
			 JUMP_ADDR_IN => int_rf_jal_to_fetch,
			 JUMP_ADDR_OUT => int_rf_jal_to_decode
		 );

	DECODE_0 : DECODE
	generic map (
			    IR_SIZE => IR_SIZE,
			    REG_ADDR_LEN => REG_ADDR_LEN,
			    OP_CODE_SIZE => OP_CODE_SIZE,
			    STAGE_SIZE => CW_STAGE1_SZ,
			    CW_SIZE => CW_SIZE,
			    BIT_NUM => BIT_NUM
		    )
	port map (
			 CLK => CLK,
			 RST  => RST,
			 BRANCH_TAKEN  => BRANCH_TAKEN,
			 MEM_WAIT => int_MEM_WAIT,
			 INSTR_IN  => int_IR_OUT,
			 PREV_STAGE_GO => next_stage_go_in(F),
			 NEXT_STAGE_GO  => next_stage_go_out(D),
			 INSTR_OUT  => IR_OUT,
			 STAGE_CW_OUT => stage1_out_s,
			 JUMP_ADDR_IN => int_rf_jal_to_decode,
			 JUMP_ADDR_OUT => JUMP_ADDR_OUT,
			 RD_OUT => RD_ADDR_ARRAY(0),
		         -- Input register addresses for forwarding unit
			 REG_A_ADDR => REG_A_ADDR,
			 REG_B_ADDR => REG_B_ADDR
		 );

	EXECUTE_0 : EXECUTE
	generic map (
			    STAGE_SIZE => CW_STAGE2_SZ,
			    REG_ADDR_LEN => REG_ADDR_LEN
		    )
	port map (
			 CLK => CLK,
			 RST  => RST,
			 BRANCH_TAKEN  => BRANCH_TAKEN,
			 MEM_WAIT => int_MEM_WAIT,
			 STAGE_CW_IN => stage2_in_s,
			 RD_IN => RD_ADDR_ARRAY(0),
			 PREV_STAGE_GO => next_stage_go_in(D),
			 NEXT_STAGE_GO  => next_stage_go_out(E),
			 STAGE_CW_OUT => stage2_out_s,
			 RD_OUT => RD_ADDR_ARRAY(1)
		 );

	MEMORY_0 : MEMORY
	generic map (
			    STAGE_SIZE => CW_STAGE3_SZ,
			    REG_ADDR_LEN => REG_ADDR_LEN
		    )
	port map (
			 CLK => CLK,
			 RST  => RST,
			 BRANCH_TAKEN  => BRANCH_TAKEN,
			 MEM_WAIT => int_MEM_WAIT,
			 STAGE_CW_IN => stage3_in_s,
			 RD_IN => RD_ADDR_ARRAY(1),
			 PREV_STAGE_GO => next_stage_go_in(E),
			 NEXT_STAGE_GO  => next_stage_go_out(M),
			 STAGE_CW_OUT => stage3_out_s,
			 RD_OUT => RD_ADDR_ARRAY(2),
			 --Memory interface
			 DRAM_ENABLE => DRAM_ENABLE,
			 DRAM_READY => DRAM_READY
		 );


	WRITE_BACK_0 : WRITE_BACK
	generic map (
			    STAGE_SIZE => CW_STAGE4_SZ,
			    REG_ADDR_LEN => REG_ADDR_LEN
		    )
	port map (
			 CLK => CLK,
			 RST  => RST,
			 BRANCH_TAKEN  => BRANCH_TAKEN,
			 MEM_WAIT => int_MEM_WAIT,
			 STAGE_CW_IN => stage4_in_s,
			 RD_IN => RD_ADDR_ARRAY(2),
			 PREV_STAGE_GO => next_stage_go_in(M),
			 STAGE_CW_OUT => stage4_out_s,
			 RD_OUT => RD_ADDR_ARRAY(3)
		 );

end dlx_cu_fsm;
