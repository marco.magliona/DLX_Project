library ieee;
use ieee.std_logic_1164.all;
use work.myTypes.all;

entity DLX is
	generic (
			IR_SIZE      : integer := 32;       -- Instruction Register Size
			PC_SIZE      : integer := 32       -- Program Counter Size
		);       -- ALU_OPC_SIZE if explicit ALU Op Code Word Size
	port (
		     CLK : in std_logic;
		     Rst : in std_logic; -- Active low

		     IRAM_ADDRESS       :  out std_logic_vector(PC_SIZE - 1 downto 0);
		     IRAM_ISSUE         :  out std_logic;
		     IRAM_READY         :  in std_logic;
		     IRAM_DATA          :  in std_logic_vector(2*IR_SIZE-1 downto 0);

		     DRAM_ADDRESS       :  out std_logic_vector(BIT_NUM-1 downto 0);
		     DRAM_ISSUE         :  out std_logic;
		     DRAM_READNOTWRITE  :  out std_logic;
		     DRAM_READY         :  in std_logic;
		     DRAM_DATA          :  inout std_logic_vector(BIT_NUM-1 downto 0)
	     );
end DLX;

architecture dlx_rtl of DLX is

  --------------------------------------------------------------------
  -- Components Declaration
  --------------------------------------------------------------------

-- Datapath
	component data_path_core is
		generic(BIT_NUM :  natural;
		ADDR_LEN        :  natural;
		ALU_OPC_SIZE    :  natural;
		REG_ADDR_LEN    :  natural);
		port(CLK               :  in std_logic;
		     RESET             :  in std_logic;
		     ENABLE            :  in std_logic;
		     CW                :  in std_logic_vector(CW_SIZE-1 downto 0);
		     JUMP_ADDR_OUT     :  out std_logic_vector(BIT_NUM-1 downto 0);
		     PC                :  in std_logic_vector(BIT_NUM-1 downto 0);
		     INSTR             :  in std_logic_vector(BIT_NUM-1 downto 0);
		     RF_WR_ADDR        :  in std_logic_vector(REG_ADDR_LEN-1 downto 0);
	     	     JUMP_ADDR_IN      :  in std_logic_vector(BIT_NUM-1 downto 0);
		     OVERFLOW          :  out std_logic;
		     BRANCH_TAKEN      :  out std_logic;
	     -- DRAM interface
		     DRAM_ADDR         :  out std_logic_vector(ADDR_LEN-1 downto 0);
		     DRAM_WRITE_DATA   :  out std_logic_vector(BIT_NUM-1 downto 0);
		     DRAM_READ_DATA    :  in std_logic_vector(BIT_NUM-1 downto 0);
		     DRAM_ENABLE       :  in std_logic;
	     -- FORWARDING signals
	     	     FOR_SEL_IN_A      : in FOR_MUX_SEL;
	     	     FOR_SEL_IN_B      : in FOR_MUX_SEL
	     );
	end component data_path_core;

-- Control Unit
	component dlx_cu is
		port (
			     CLK                : in  std_logic;
			     Rst                : in  std_logic;
		             IR_IN              : in  std_logic_vector(2*IR_SIZE - 1 downto 0);
		     	     JUMP_ADDR_IN       : in std_logic_vector(BIT_NUM-1 downto 0);
		      	     IR_OUT             : out std_logic_vector(IR_SIZE -1 downto 0);
		     	     PC_OUT       : out std_logic_vector(BIT_NUM-1 downto 0);
			     RF_WR_ADDR : out std_logic_vector(REG_ADDR_LEN-1 downto 0);
			     JUMP_ADDR_OUT : out std_logic_vector(BIT_NUM-1 downto 0);
			     OVERFLOW : in std_logic;
			     BRANCH_TAKEN : in std_logic;
			     CW : out std_logic_vector(CW_SIZE-1 downto 0);
		     	     IRAM_ENABLE: out std_logic;
			     IRAM_READY: in std_logic;
			     IRAM_ADDRESS : out std_logic_vector(BIT_NUM-1 downto 0);
			     DRAM_READY: in std_logic;
			     DRAM_ENABLE: out std_logic;
	     		     -- FORWARDING signals
	     	             FOR_SEL_IN_A: out FOR_MUX_SEL;
	     	             FOR_SEL_IN_B: out FOR_MUX_SEL
		     );
	end component dlx_cu;


----------------------------------------------------------------
-- Signals Declaration
----------------------------------------------------------------

	signal CW                : std_logic_vector(CW_SIZE-1 downto 0);
	signal JUMP_ADDR_DP_TO_CU: std_logic_vector(BIT_NUM-1 downto 0);
	signal PC                : std_logic_vector(BIT_NUM-1 downto 0);
	signal INSTR             : std_logic_vector(2*BIT_NUM-1 downto 0);
	signal RF_WR_ADDR        : std_logic_vector(REG_ADDR_LEN-1 downto 0);
	signal JUMP_ADDR_CU_TO_DP: std_logic_vector(BIT_NUM-1 downto 0);
	signal OVERFLOW          : std_logic;
	signal BRANCH_TAKEN      : std_logic;
-- DRAM interface
	signal DRAM_ADDR         : std_logic_vector(ADDR_LEN-1 downto 0);
	signal DRAM_WRITE_DATA   : std_logic_vector(BIT_NUM-1 downto 0);
	signal DRAM_READ_DATA    : std_logic_vector(BIT_NUM-1 downto 0);
	signal int_DRAM_ENABLE   : std_logic;
-- FORWARDING signals
	signal FOR_SEL_IN_A : FOR_MUX_SEL;
	signal FOR_SEL_IN_B : FOR_MUX_SEL;

	signal IR_OUT_FROM_CU 	: std_logic_vector(IR_SIZE-1 downto 0);
begin

	DRAM_ISSUE <= int_DRAM_ENABLE;
	-- IRAM signals
	INSTR <= IRAM_DATA;
	-- IRAM_ADDRESS <= "00"&PC(BIT_NUM-1 downto 2) ;

-- Control Unit Instantiation
	dlx_cu_0 : dlx_cu
	port map (
			 CLK            =>  CLK,
			 Rst            =>  Rst,
			 IR_IN          =>  INSTR,
			 JUMP_ADDR_IN   =>  JUMP_ADDR_DP_TO_CU,
			 IR_OUT 	=>  IR_OUT_FROM_CU,
			 PC_OUT         =>  PC,
			 RF_WR_ADDR     =>  RF_WR_ADDR,
			 JUMP_ADDR_OUT  =>  JUMP_ADDR_CU_TO_DP,
			 OVERFLOW       =>  OVERFLOW,
			 BRANCH_TAKEN   =>  BRANCH_TAKEN,
			 CW             =>  CW,
			 IRAM_ENABLE    =>  IRAM_ISSUE,
			 IRAM_READY     =>  IRAM_READY,
			 IRAM_ADDRESS   =>  IRAM_ADDRESS,
			 DRAM_READY     =>  DRAM_READY,
			 DRAM_ENABLE    =>  int_DRAM_ENABLE,
			 FOR_SEL_IN_A   =>  FOR_SEL_IN_A,
			 FOR_SEL_IN_B   =>  FOR_SEL_IN_B
		 );

-- DataPath Instantiation
	data_path_core_0 : data_path_core
	generic map (
			    BIT_NUM  => BIT_NUM,
			    ADDR_LEN         => ADDR_LEN,
			    ALU_OPC_SIZE     => ALU_OPC_SIZE,
			    REG_ADDR_LEN     => REG_ADDR_LEN )
	port map (
			 CLK                => CLK,
			 RESET              => Rst,
			 ENABLE             => '1',
			 CW                 => CW,
			 JUMP_ADDR_OUT      => JUMP_ADDR_DP_TO_CU,
			 PC                 => PC,
			 INSTR              => IR_OUT_FROM_CU,
			 RF_WR_ADDR         => RF_WR_ADDR,
			 JUMP_ADDR_IN       => JUMP_ADDR_CU_TO_DP,
			 OVERFLOW           => OVERFLOW,
			 BRANCH_TAKEN       => BRANCH_TAKEN,
	     -- DRAM interface
			 DRAM_ADDR          => DRAM_ADDR,
			 DRAM_WRITE_DATA    => DRAM_WRITE_DATA,
			 DRAM_READ_DATA     => DRAM_READ_DATA,
			 DRAM_ENABLE        => int_DRAM_ENABLE,
			 FOR_SEL_IN_A  	    => FOR_SEL_IN_A,
			 FOR_SEL_IN_B  	    => FOR_SEL_IN_B
		 );

DRAM_ADDRESS <= DRAM_ADDR;
DRAM_DATA <= DRAM_WRITE_DATA when CW(POS_CW_DRAM_WE)='1' else (others => 'Z');
DRAM_READNOTWRITE <= '1' when CW(POS_CW_DRAM_WE)='0' else '0' when CW(POS_CW_DRAM_WE)='1' else '0';
DRAM_READ_DATA <= DRAM_DATA(BIT_NUM-1 downto 0);
end dlx_rtl;
