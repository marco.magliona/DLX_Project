nop
# R1 <- 1
addi r1,r0,#1
# R2 <- R1+5
addi r2,r1,#5
addi r3,r0,#7
# R1 <- R2+R2
add r1,r2,r2
jal another_jump
# IMPORTANT
# The jal instruction stores not the PC of the following instruction but that of 2 instructions
# after, so to avoid the execution of the instructions at the brach delay slot.
# The nop here is only a dummy to fill the empty slot
nop
nop
# Should not be done at first cycle but after the JR
add r2,r2,r2
add r3,r2,r1
j end
nop
nop
another_jump:
addi r2,r1,#30
addi r3,r0,#20
# Jump at the instruction after the Branch delay slot
addi r31,r31,#4
jr r31
nop
nop
end:
nop
nop
nop
nop
#
