# This test is for the "loop cache"
# Instructions in the loop should be fetched only once and then "protected" until
# the loop is finished. All the instructions in the loop, after the first iteration, 
# are issued immediately (one per clock cycle).
# Very stupid example that writes in r1 the number of iterations remaining.
# The loop is little enough to be contained in the instruction queue
# Please notice the two nops in the branch delay slot
addi r5,r0,#4
nop
loop_here:
add r1,r5,r0
subi r5,r5,#1
bnez r5,loop_here
nop
nop
addi r2,r0,#1
nop
nop
nop
nop
