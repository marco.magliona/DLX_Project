# This test tries the forwarding for each stage
# Since the instructions are fetched in couples
# two instructions in the same couple are normally
# issued one after the other, otherwise they need to be
# fetched from IRAM (if the queue is empty like here).
# The nop here is to force the forwarding from the memory
# phase
addi r1,r0,#1
# Test forwarding from execute
addi  r5,r1,#2
# Test forwarding from write back
addi  r6,r1,#3
# No forwarding
addi  r7,r1,#4
nop
addi r2,r0,#2
# Test forwarding from memory
add  r8,r0,r2
nop
nop
nop
nop
nop
nop
nop
nop

