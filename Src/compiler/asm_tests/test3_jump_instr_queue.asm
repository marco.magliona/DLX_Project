nop
nop
addi r4,r0,#1
addi r5,r0,#0
addi r6,r0,#6
# Uncomment the following nop to test the instr queue behaviour when a j instruction is provided as
# the first instruction (32 lowest bits) in a IRAM word. If it is commented the jump is the second
# instruction in the word (32 highest bits).
# nop
j lab1
# should not be done
addi r7,r0,#7
addi r8,r0,#8
lab1:
addi r7,r0,#8
addi r8,r0,#9
# this should be taken
bnez r4,lab2
nop
nop
# this should not be done
addi r6,r0,#10
nop
lab2:
addi r6,r0,#11
nop
jal lab3
addi r5,r0,#3
subi r6,r0,#4
nop
nop
lab3:
addi r5,r0,#5
nop
nop
nop
nop
# Do some memory operations to fill the queue
sw 4(r0),r5
sw 5(r0),r4
sw 6(r0),r3
lw r10,4(r0)
bnez r5,some_jump_already_in_queue
nop
nop
# this should not be done
addi r5,r0,#0
some_jump_already_in_queue:
addi r5,r0,#1
nop
nop
nop
nop
