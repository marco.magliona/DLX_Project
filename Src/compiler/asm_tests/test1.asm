# Some random instructions for an general test
# Testings: jumps (conditional or not, backward and forward, from register), pipeline behaviour and forwarding
# Memory reading and writing
nop
# R1 <- 1
addi r1,r0,#1
# R2 <- R1+5
addi r2,r1,#5
addi r3,r0,#6
# R1 <- R2+R2
add r1,r2,r2
j jump_test
# This operation should not be done
subi r3,r1,#5
jump_test:
subi r3,r1,#4
addi r4,r0,#1
bnez r4,r4_not_zero
nop
nop
# This operation should not be done
addi r5,r0,#10
r4_not_zero:
# This should be directly executed after the bnez
addi r5,r0,#20
beqz r6,test_pipeline
nop
nop
addi r1,r0,#10
test_pipeline:
addi r1,r0,#2
addi r2,r0,#3
addi r3,r0,#4
addi r4,r0,#5
addi r5,r0,#6
addi r6,r0,#7
sub r7,r2,r1
and r8,r2,r1
jal another_jump
addi r10,r0,#7
another_jump:
addi r10,r0,#8
# Jump back to the instruction after the jal
jr r31
sw 4(r0),r5
sw 5(r0),r4
sw 6(r0),r3
lw r10,4(r0)
nop
nop
nop
# Test jump back
j jump_test
nop
nop
nop
nop
