#!/bin/sh

# This script generates the corresponding tex files from the diagram
# Requires Dia
# Run from Report path

rm -f chapters/diagrams/*.tex
dia -O chapters/diagrams -t pgf-tex ../Design/*.dia
# Dirty hack to fix the missing font color in the latex export module
find ./chapters/diagrams -type f -name \*.tex -exec sed -i 's/anchor/color=dialinecolor,anchor/g' {} \;
