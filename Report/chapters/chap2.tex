\chapter{Structure and logic blocks}
\label{chap:2}

\section{Global constants and functions overview}
\subsection{\emph{myTypes} package}
Wherever possible all the constants and generics used in the design have been grouped in the package \emph{myTypes} inside the file \emph{000-globals.vhd} in the root of \emph{DLX\_vhd}.
In the file are contained (following the order of the file itself) :
\begin{itemize}
	\item General constants including number of bits of the operands, all sizes related to opcodes, register address length, and size of the instruction queue
	\item ALU opcodes mnemonic name vs bit representation
	\item Number of control signals in each stage (decode, execute, ...) in the form of \textbf{CW\_IND\_STAGEn\_SZ} and number of control signals in a specific stage plus those in the following stages in the form of \textbf{CW\_STAGEn\_SZ}
	\item Position of the control signals in each stage. For instance the \emph{conditional jump} control signal is in position 4 in the std\_logic\_vector of the control signals for the execute stage, see corresponding VHDL line in \autoref{lst:ff_cond}
	\item Position of the control signals in the control word (CW in the code). For instance the \emph{conditional jump} control signal is in position 9 of the whole control word driven by the control unit. See corresponding VHDL line in \autoref{lst:cw_cond}
	\item Natural values of the opcodes in hexadecimal format OPC\_NAT\_<opcode>. See \autoref{lst:op_nat_ex}
	\item \emph{std\_logic\_vector} equivalent of the values of the opcodes OPC\_<opcode name>. See \autoref{lst:op_ex}
	\item Forward muxes selection signals mnemonics
	\item Forward signals type declaration
\end{itemize}
\inputvhdl{000-globals.vhd}{59}{59}{1}{}{lst:ff_cond}
\inputvhdl{000-globals.vhd}{80}{80}{1}{}{lst:cw_cond}
\inputvhdl{000-globals.vhd}{98}{98}{1}{}{lst:op_nat_ex}
\inputvhdl{000-globals.vhd}{134}{134}{1}{}{lst:op_ex}
\subsection{Auxiliary functions in the \emph{utils} package}
\label{subsec:utils}
This package contains the functions used to create the control word starting from the individual control signals and their expected positions (specified in \emph{myTypes} package). It is only used in the memory inside the control unit. This file \emph{001-aux\_func.vhd} is in the root of the sources folder because before the CU was ready to be connected to the datapath, it contained some functions to generate the signals for the datapath's testbench. To see the old files with those test functions run \shellcmd{git show 38a9db67b450da02701bad362de8275d4cefebb5:Src/DLX\_vhd/001-aux\_func.vhdl} .

\section{Top level view}
\label{sec:top_level}
The DLX is internally divided in two main blocks : \emph{Control unit} and \emph{Datapath}. The conceptual internal and external signals are showed in \autoref{fig:top}. In \autoref{fig:top_hier}, the hierarchical relationships between the entities can be seen. \dia{TOP}{Top level view}{fig:top}{2}{0.8}

The hierarchy view is automatically generated using Doxygen and its configuration file is \emph{Doxyfile\_config} in \emph{Doc} folder as well as the generated files in \emph{latex} subfolder. The configuration file can be used with \emph{Doxywizard}, just open it and run to update the schemes.

\begin{figure}[H]
	\begin{center}
		\resizebox{\textwidth}{!}{\includegraphics{../Doc/latex/class_d_l_x__inherit__graph.pdf}}
		\caption{Hierarchy of the VHDL files (ZOOM IN if needed). The title of each block is the entity name followed bellow by the generics, input/output signals and the packages included. The arrows points to the other entities used as components by that block.}
		\label{fig:top_hier}
	\end{center}
\end{figure}

\section{Datapath}
The Datapath has undergone lots of modifications during the development. The original concept diagram of the datapath can be seen in \autoref{fig:orig_datapath} and the final one in \autoref{fig:final_datapath}.
\dia{diagram_pre_development}{Original datapath design}{fig:orig_datapath}{0}{0.8}
\dia{diagram}{Final datapath design (ZOOM IN if needed). \textcolor[RGB]{0,128,0}{Green} signals are from the CU, \textcolor[RGB]{0,18,255}{blue} signals are for forwarding, \colorbox[RGB]{0,255,244}{light blue} blocks are embedded inside other blocks, \colorbox[RGB]{191,191,191}{grey} boxes are not inside the datapath}{fig:final_datapath}{0}{1.0}

In the original datapath, IRAM and DRAM where inside the unit and made of registers as the register file (RF in the pictures) just to test the path behaviour. As soon as the "real" memories have been introduced, the need of a more complex control logic for them invited me to move the interface directly in the control unit whereas possible. The fetch phase has been completely moved to the control unit and it is now managed directly out of the datapath which receives the instruction whenever is ready. For simplicity the whole instruction is forwarded from the control unit, but the datapath only uses the source registers addresses contained in it. The destination jump address is received by the control unit and it is not computed by the ALU as it was in the original scheme. Some more little changes has been necessary introduced to comply with the new features added, like \emph{forwarding} related signal in blue and muxes, as well as some more register files to synchronize data between the various stages. All the changes made to the datapath can be seen executing the command \shellcmd{git log -- Src/DLX\_vhd/a.b*} from the top level directory. 

\subsection{Register file}
\label{sec:reg_file}
The register file is a simple register with two independent reading ports, one writing port and an additional special writing port. The two reading ports have different read enable signals, addresses and data outputs, the writing port is instead made of a writing enable signal, destination address and data input. The additional writing port has been added specifically for the jal instruction: its writing address is hardwired to the last available register and has its own data input. A general enable signal is present which is, like all the other control signal in this block but the RST, active HIGH. Trying to write at the same time in R31 (the last one) using the normal write port and the jal port has not been tested and its discouraged even if the jal write has the priority over the other as can be seen in the code in \emph{a.b.e-REGISTER\_FILE} in \autoref{lst:reg_file}.
The register R0 at address 0 is hardwired to LOW so it cannot be written and a writing operation on it is meaningless: during synthesis Synopsys give a warning that R0 is wired to GND. The relevant code is in \autoref{lst:reg_file} line 2. 
\inputvhdl{a.b-DataPath.core/a.b.e-REGISTER_FILE/registerfile_gen_zero.vhd}{45}{52}{4}{VHDL code responsible of the writing in the register file}{lst:reg_file}

\subsection{Immediate extension}
\label{subsec:imm}
This unit was more complex in the previous stages of development but has been gradually simplified. The actual version has been stripped of mostly all of the previous logic and can only perform integer extension signed or unsigned (padding with zeros or sign extension). The original old version can be seen giving the command \shellcmd{git show dlx\_basic\_syn:Src/DLX\_vhd/a.b-DataPath.core/a.b.d-IMM\_EXT.vhd}.

\dia{imm_ext}{Internal schematic of the immediate extension unit}{fig:imm_ext}{0}{0.5}

\subsection{Branch unit}
The branch unit is another module that has undergone lots of modifications, starting from a clocked,latched design to a fully combinational one (Synopsys introduces some output registers in any case). The complete history of this file can be obtained with \shellcmd{git log -- a.b.c-BRANCH.vhd} executed from the folder a.b-DataPath.core. The latest changes are because of most of the jump logic has been directly inserted in the instruction queue module in the control unit. The circuit here is too complex to be redrawn so a screenshot from Synopsys can be seen below in \autoref{fig:branch}.

\inputfigure{branch_unit_screen.png}{Synopsys produced branch unit circuit. The input signals on the left, from top to bottom, are: JUMP, J\_ADDR, BRANCH\_REG, COND\_JUMP, REG\_VALUE, EQ\_ZERO and RST. The output signals on the right are BRANCH\_TAKEN and PC\_OUT.}{fig:branch}{0.4}

The input signals are:
\begin{itemize}
	\item JUMP: HIGH if the instruction is a jump, behaves like an enable signal
	\item J\_ADDR: the jump destination address
	\item BRANCH\_REG: HIGH if the destination address is stored in a register (JR instruction)
	\item COND\_JUMP: HIGH if conditional jump (BNEZ, BEQZ)
	\item REG\_VALUE: output of the register file, if BRANCH\_REG is HIGH this is the jump destination address otherwise this is the value to be compared to zero in conditional jumps
	\item EQ\_ZERO: condition to check for the jump, HIGH if the unit should jump when REG\_VALUE equals to zero, LOW if the unit should jump when REG\_VALUE does not equal to zero
	\item RST: RESET signal, active LOW
\end{itemize}

The output signals are:
\begin{itemize}
	\item PC\_OUT: the new program counter if the jump is taken
	\item BRANCH\_TAKEN: HIGH if the branch is taken
\end{itemize}

\subsection{ALU}
This module is the most complex entity in the Datapath and contains the critical path that limit the maximum clock reachable by the design (see \autoref{chap:syn}). The adder is a Pentium 4 adder and the whole ALU is T2 alike. The diagram used for the design can be seen in \autoref{fig:ALU} as well as the opcodes corresponding to each operation. The LSB of the OPCODE is directly used as carry in and selection signal for B/not(b) input. If the opcode LSB is one, the output of the ALU is a subtraction, otherwise is an addition of the two inputs. The shifter has been directly dropped here and it is exactly the one provided which uses the functions in the library. The actual implementation of the mux at the bottom of \autoref{fig:ALU} can be seen in the following piece of code: \inputvhdl{a.b-DataPath.core/a.b.b-ALU.vhd}{50}{62}{1}{VHDL code of the mux inside the ALU responsible of the decoding}{lst:alu}
The valid opcodes accepted by the ALU are:
\begin{enumerate}
\item \(ADD \Rightarrow 00000\) 
\item \(SUB \Rightarrow 00001\)
\item \(AND \Rightarrow 00010  \)
\item \(OR \Rightarrow 00100   \)
\item \(XOR \Rightarrow 00110  \)
\item \(A>=B \Rightarrow 01001 \)
\item \(A>B \Rightarrow 01011  \)
\item \(A<=B \Rightarrow 01101 \)
\item \(A<B \Rightarrow 01111  \)
\item \(A=B \Rightarrow 10001  \)
\item \(A \neq B \Rightarrow 11001\)
\item Shift and rotate operations are in the form \emph{1xy1z} where
	\begin{itemize}
		\item \emph{z} is \(LOGIC/\overline{ARITH}\)
		\item \emph{y} is \(LEFT/\overline{RIGHT}\)
		\item \emph{x} is \(SHIFT/\overline{ROTATE}\)
	\end{itemize}
\end{enumerate}
\dia{ALU}{ALU designed schematics (ZOOM IN if needed)}{fig:ALU}{0}{1.0}

\subsection{Muxes}
All the muxes in the datapath, excluding the ones inside other components, have been implemented directly, without an entity but using the construct when/else. They are all grouped inside the file \emph{a.b-DataPath.vhd} and are reported below in \autoref{lst:muxes}. They directly use the control signals sent by the CU and inside the vector \emph{CW}. All the constants used in this part can be found inside the file \emph{000-globals.vhd} in the package \emph{myTypes}.
\inputvhdl{a.b-DataPath.vhd}{240}{262}{1}{Muxes instances}{lst:muxes}

\section{Control unit}
The control unit is the design part that has been modified the most. /FIXME All the changes related to it can be obtained with \shellcmd{git log -- a.a*} launched from \emph{DLX\_vhd} folder. The original idea was a central FSM in charge of controlling some registers, one for each stage of the pipeline containing the control signals for that specific stage. After various changes I've switched to a five FSMs hierarchy. Some of the FSMs were quite simple and constituted only of 2 states, reset and normal operation, so they have been simplified with a single process sensible to clock and reset signals: this is the case of the \textbf{execute} and \textbf{write back} units. The organization of the CU can be seen in \autoref{fig:top_cu_dia} and the dependencies between its parts in \autoref{fig:top_cu}. All the FSMs are one-process. I've opted for this solution to have a fully synchronous behaviour and usually they are better in avoiding unneeded latches.

\dia{control_unit_top}{Control unit top level view}{fig:top_cu_dia}{0}{1.0}

\begin{figure}[H]
	\begin{center}
		\resizebox{\textwidth}{!}{\includegraphics{../Doc/latex/classdlx__cu__inherit__graph.pdf}}
		\caption{Hierarchy of the CU VHDL files (ZOOM IN if needed). The title of each block is the entity name followed bellow by the generics, input/output signals and the packages included. The arrows points to the other entities used as components by that block.}
		\label{fig:top_cu}
	\end{center}
\end{figure}

\subsection{Instruction queue}
\label{subsec:instr_queue}
The instruction queue is the most complex unit of the design. It is basically divided in three, mostly independent, processes. This unit can be considered a pipeline itself with a memory element (three circular buffers). Each block in \autoref{fig:instr_queue} is one of the processes mentioned before and in the code they are called \emph{FIFO\_PROCESS\_GET\_FROM\_IRAM}, \emph{FIFO\_PROCESS\_CHECK\_JUMP} and \emph{FIFO\_PROCESS\_SPILL}.

\dia{instr_queue}{Instruction queue internal structure (Zoom in if needed)}{fig:instr_queue}{0}{1.0}
\subsubsection{First stage}
In this phase the IRAM is enabled and all the following stages are stalled until it is ready. When the IRAM outputs its data, this process checks if the circular buffer is not full and eventually passes the received data to the second stage
\subsubsection{Second stage}
\label{subsubsec:instr_queue_second}
This process contains most of the logic of the whole unit, including:
\begin{itemize}
	\item Reset circular buffer on reset
	\item Update head pointer (which cell in the buffer should be written in)
	\item Update the program counter/IRAM address
	\item Manage unconditional jumps which is done immediately (the jump instruction is executed here and not stored in the buffer) and eventually compute and update the program counter
	\item Manage the JAL instruction: compute the destination address, store in the buffer, set write enable for the additional signals in the register file (See \autoref{sec:reg_file})
	\item Manage conditional jumps: compute and store the destination address is the jump is taken (it will be passed to the branch unit in the datapath) and in case of a backward jump, mark that position in the buffer as a potential loop first instruction and if the loop is bigger than the buffer size, flush the memory and start fetching again from IRAM
\end{itemize}
\subsubsection{Circular buffer}
In the circular buffer are stored the instructions and other informations after they are fetched from the IRAM and before they are issued to the fetch unit. It is implemented as three different arrays of registers plus a std\_logic\_vector but since they are always modified together, they can be considered as one bigger buffer. In each slot are stored:
\begin{itemize}
	\item Two consecutive instructions as given by the IRAM (\emph{instr\_queue} in the VHDL code)
	\item Their corresponding program counters (\emph{pc\_queue} in the code)
	\item Their destination addresses and the if one of them need to be written in the register file because of the JAL (\emph{jump\_queue} in the code)
	\item If the instruction is a backward conditional jump (\emph{backward\_cond\_jump\_in\_instr} in the code)
\end{itemize}
The size of the buffer is specified by the generic \textbf{QUEUE\_SIZE} defined in the \emph{000-globals.vhd} file and should always be a power of two.
\subsubsection{Third stage}
This stage is the interface with the fetch phase FSM and has different behaviours depending of the buffer status, conditional jump or normal instruction. This process need to check if the buffer is empty, if the fetch unit asked for a new instruction and if it is ready, output the next "not yet issued" instruction and associated program counter, jump address(if any), jal write control signal and set the output ready signal. The scenario is different when a conditional jump has been executed and taken. The unit needs to check where the requested instruction is in the buffer, update the spilling pointer (tail) and output it.
\subsection{FSMs hierarchy and pipeline stages}
Each stage in the pipeline corresponds to a specific control unit, FSM or a process for the simplest stages. One stage can go on only if the previous one has finished and the pipeline is working normally (no memory wait). A \textbf{NEXT\_STAGE\_GO} is risen up to let the following unit know that can process its inputs. This feedthrough mechanism is the same for all the stages. All the units but the fetch have an additional signal \textbf{RD\_OUT} for the forwarding unit: each one of them is the destination address of the instruction at that specific stage. Let's suppose that the instruction \textbf{ADDI R1,R0,\#3} is at the decode stage, the decode unit puts the address of \textbf{R1} on \textbf{RD\_OUT}. In this way it is possible to know exactly where the new content of a register is in the pipeline.
\subsubsection{Fetch}
The fetch unit interfaces directly with the instruction queue. Its main job is getting the instructions (in couples), separate and issue them to the next stage. This unit manages the pipeline and must stall it if needed. I've moved the memory stall logic in this unit to simplify the pipeline flow (the stalls due to memory are removed by the memory unit as soon as the DRAM is ready to go on). All the signals that need to be issued in this and the following stages need to pass through this unit: in this way is easier to keep all synchronized. An example of this choice can be seen in the \textbf{JUMP\_ADDR} signals that will be then passed to the decode unit. The FSM can be roughly divided in three operating modes:
\begin{itemize}
	\item Reset: all signals to zero
	\item After reset: ask for a new instruction immediately and store it internally
	\item Normal: keep the internal memory full (two instructions) and issue them one at time
	\item Identify a memory operation an wait for its completion 
\end{itemize}

\subsubsection{Decode}
The decode unit convert the opcodes to actual control signals. This is done in different steps: ALU opcode generation, immediate unit sign setting and the other control signals. The write enable signal for the destination address (JAL instruction) is only propagated from the fetch instruction and it is not modified. The ALU opcode is generated by the process \textbf{ALU\_OP\_GEN} which is a list of if/then/else constructs and its value is all zeros in case of reset or unrecognized opcode (see \autoref{lst:op_gen}).
\inputvhdl{a.a-CU.core/a.a.e-DECODE.vhd}{61}{107}{1}{}{lst:op_gen}
The sign control signals is stored whenever possible in the microcode memory and this is done for all but the \emph{R type} instructions which are grouped altogether in a single entry. In this case the sign control signal is set to HIGH if needed by the FSM itself.
\paragraph*{Microcode memory}
\label{subsubsection:microcode}
This unit \textbf{CU\_MEM\_ARRAY} contains the association between opcode and control signals. Not all instructions are here like the j, jal which are decoded in the instruction queue. The \emph{R type} instructions are grouped in a single entry since they have always the same opcode. The \emph{I type} instructions can be separated in signed and unsigned instructions which have mostly the same resulting control signals and all the other instructions have their own entry here. The control word signals are generated using the function in \emph{001-aux\_func.vhd} discussed in \autoref{subsec:utils}.
\subsubsection{Execute}
The execute unit is quite simple two states are present: reset and normal. The code should be self explanatory.
\inputvhdl{a.a-CU.core/a.a.f-EXECUTE.vhd}{22}{41}{0}{Execute stage logic}{}
\subsubsection{Memory}
This unit is responsible of the DRAM control and ready signals (data and address are managed by the datapath). It needs to identify a memory operation and, if this is the case, enable the DRAM, wait until it answers (DRAM ready signal) and finally disable the enable.
\subsubsection{Write back}
The write back stage is very similar to the execute one. The only difference is that the \textbf{NEXT\_STAGE\_GO} has been removed since this is the last stage of the pipeline.
\subsubsection{Forwarding unit}
The forwarding unit aim is to find in the pipeline the inputs operands of the current operation. It takes as inputs the registers' addresses of the operands for the current operation and the destination address of the operation done at the decode, execute,memory and write back phases. Each of the required registers address is compared to each of the destination registers address from the various stages: if a match is found it sets the corresponding mux in the datapath to "forward" the value not yet written in the RF as operand for the ALU. Forwarding is stalled when the pipeline is stalled due a memory operation. This module outputs its response at the next clock cycle so it needs to know the address of the operation at the decode phase, which has yet to be executed: at the next clock cycle that operation will be in the execute phase so the result is ready and can be used. The situation is similar for the write back stage: the operand is been written so the output data of the register file is not correct and needs to be "bypassed".
